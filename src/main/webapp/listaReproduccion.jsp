<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="servicios.Lista"%>
<%@ page import="servicios.Video"%>
<%@ page import="servicios.Usuario"%>
<%@ page import="java.util.List"%>
<jsp:include page="header.jsp"></jsp:include>


<!DOCTYPE html>
<html>
<head class="listaReproduccion">
<meta charset="UTF-8">
<title>Lista de Reproducción</title>
</head>
<body class="listaReproduccion">
	<jsp:include page="sideBar.jsp"></jsp:include>
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		<div class="single-grid-right">
			<div class="row" style="display: flex; justify-content: center">
				<div class="col-md-7 col-xs-12">
					<h1 class="listaReproduccion">
						UyTube&nbsp;<span style="color: black;">| <%=request.getAttribute("nombreLista")%></span>&nbsp;<span
							style="color: black; font-size: 100%;">| <%=((Usuario) request.getSession().getAttribute("usuarioLogueado")).getNickname()%>
						</span>
					</h1>
				</div>
				<div class="col-md-5 col-xs-12">
					<form class="uploadvideo" action="modificarLista" method="get">
						<input class="btn btn-danger" type="submit" name="privacity"
							value="<%=request.getAttribute("privacidad")%>"> <input
							type="hidden" name="idListaRepro"
							value="<%=request.getAttribute("idListaRepro")%>">
					</form>

				</div>
			</div>

			<p style="font-size: 20px; padding-left: 20px; color: grey;">Lista
				de reproducción</p>
			<%
				for (Video v : (Video[]) request.getAttribute("listaRepro")) {
			%>
			<div class="animated-grids">
				<div style="padding-left: 10px; padding-bottom: 10px;"
					class="col-md-3 resent-grid recommended-grid slider-first">
					<div class="resent-grid-img recommended-grid-img">
						<%
							String[] idYoutube = v.getUrl().split("/");
						%>
						<a href="ver?id_video=<%=v.getId()%>"><img
							src="https://img.youtube.com/vi/<%=idYoutube[idYoutube.length-1]%>/hqdefault.jpg"
							alt="" /></a>
						<div class="time small-time slider-time">
							<p><%=v.getDuracion()%></p>
						</div>
						<div class="clck small-clck">
							<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
						</div>
					</div>
					<div class="resent-grid-info recommended-grid-info">
						<h5>
							<a href="ver?id_video=<%=v.getId()%>" class="title"><%=v.getNombre()%></a>
						</h5>
						<div class="slid-bottom-grids">
							<div class="slid-bottom-grid">
								<p class="author">
									<a href="#" class="author"><%=v.getCanal().getNombre()%></a>
								</p>
							</div>
							<div class="slid-bottom-grid slid-bottom-right">
								<%--<p class="views views-info"><%=v.getVisitas()%> visitas</p> --%>
									<!--Descomentar la linea de arriba cuando se pase los videos por servicio y no models -->
									<p class="views views-info"><%=v.getVisitas()%> visitas</p>
							</div>
							<div class="clearfix"></div>
						</div>
					</div>
				</div>
				<%
					}
				%>
			</div>
		</div>
	</div>
</body>
</html>
<script src="/web/resources/js/bootstrap.min.js"></script>
