<%-- <%@ page language="java" contentType="text/html; charset=ISO-8859-1" --%>
<%-- 	pageEncoding="ISO-8859-1"%> --%>
<%@ page import="servicios.Lista"%>
<%@ page import="servicios.Categoria"%>
<%@ page import="java.util.List"%>

<!DOCTYPE html>
<html>
<head>
<META http-equiv="Content-Type" content="text/html" charset="utf-8">
<title>UyTube | Sube tu video</title>
<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="/web/resources/js/snippetVideo.js"></script>
</head>
<jsp:include page="header.jsp"></jsp:include>

<body class="uploadvideo">
	<jsp:include page="sideBar.jsp"></jsp:include>
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		<div class="col-sm-8 single-left">
			<h1 class="uploadvideo">
				UyTube&nbsp;<span style="color: black;">| Coloca los datos de
					tu video</span>
			</h1>
			<jsp:include page="searchYoutube.html"></jsp:include>
			<form id="formSnippet" class="uploadvideo">
				<div class="row">
					<div class="col-md-6 col-xs-12">
						<label>Link del video&nbsp;&nbsp;<a href="#"
							onclick="document.getElementById('app').style='display:block'">
								Busca en YT
						</a></label> <input id="url" type="text" name="url" required
							class="form-control" />
					</div>
					<div class="col-md-6 col-xs-12">
						<label>Descripcion</label> <input id="descripcion" type="text"
							name="descripcion" class="form-control" />
					</div>
					<div class="col-md-6 col-xs-12">
						<label>Privacidad</label> <select style="width: 100%"
							id="privacidadVideo" name="privacidad" class="form-control">
							<option selected value="publico">P�blico</option>
							<option value="privado">Privado</option>
						</select>
					</div>
					<div class="col-md-6 col-xs-12">
						<label>Categoria</label> <select style="width: 100%"
							id="categoria" name="categoria" class="form-control">
							<option selected value="Sin Categoria">Sin Categoria</option>
							<%
								for (Categoria c : (Categoria[]) request.getAttribute("categorias")) {
							%>
							<option value=<%=c.getId()%>>
								<%=c.getNombre()%></option>
							<%
								}
							%>
						</select>
					</div>
					<div class="col-md-12 col-xs-12">
						<br> <input id="formSnippet" style="width:100%" class="submit" type="submit"
							value="Sube tu video">
					</div>
					<div class="col-md-6 col-xs-6">
						<br> <input class="boton" type=button style="width:100%" value="Atr�s"
							onclick="history.go(-1)">
					</div>
					<div class="col-md-6 col-xs-6">
						<br> <a href="/web/home"><input class="boton" style="width:100%" type=button
							value="Ir al inicio" onclick="window.location.href='/web/home"></a>
					</div>
			</form>

		</div>
		<script src="/web/resources/js/snippetVideo.js"></script>
	</div>
	<div class="clearfix"></div>
	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="/web/resources/js/bootstrap.min.js"></script>
	<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
</body>
</html>