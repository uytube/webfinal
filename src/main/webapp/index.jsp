<%@ page import="java.util.List"%>
<%@ page import="servicios.Video"%>
<%@ page import="servicios.Categoria"%>
<jsp:include page="header.jsp"></jsp:include>
<body>
	<jsp:include page="sideBar.jsp"></jsp:include>
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		<div class="container-fluid">
			<div class="top-search mobile-search">
				<form class="" action="/web/search"
					method="get">
					<input type="text" class="form-control" name="q"
						placeholder="Search..."> <input type="hidden"
						name="alfabeticamente"> <input type="hidden" name="fecha">
					<input type="submit" value=" ">
				</form>
			</div>
			<div class="recommended">
				<div class="recommended-grids">
					<%
					if(request.getAttribute("videos")!=null){
					for (Video v : (Video[]) request.getAttribute("videos")) { 
					%>
					<div class="col-md-3 resent-grid recommended-grid">
						<div class="resent-grid-img recommended-grid-img">
							<%
								String[] idYoutube = v.getUrl().split("/");
							%>
							<a href="ver/<%=idYoutube[idYoutube.length - 1]+"@"+v.getId()%>" class="title"><img
								src="https://img.youtube.com/vi/<%=idYoutube[idYoutube.length - 1]%>/hqdefault.jpg"
								alt="" /></a>
							<div class="time small-time">
								<p><%=v.getDuracion()%></p>
							</div>
							<div class="clck small-clck">
								<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
							</div>
						</div>
						<div
							class="resent-grid-info recommended-grid-info video-info-grid">
							<h5>
								<a href="ver/<%=idYoutube[idYoutube.length - 1]+"@"+v.getId()%>" class="title"><%=v.getNombre()%></a>
							</h5>
							<ul>
								<li>
									<p class="author author-info">
										<a
											href="/web/canal?canal=<%=v.getCanal().getUsuario().getNickname()%>"
											class="author"><%=v.getCanal().getNombre()%></a>
									</p>
								</li>
								<li class="right-list">
									<p class="views views-info"><%=v.getVisitas() %> Visitas</p>
								</li>
							</ul>
						</div>
					</div>
					<%
					}
						}
					%>
				</div>
			</div>
		</div>
	</div>
	<div class="clearfix"></div>
	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="/web/resources/js/bootstrap.min.js"></script>
	<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
</body>
<jsp:include page="floatingVideo.html"></jsp:include>

</html>
<style>
	@media(min-width:768px){
		.mobile-search{
		display:none;
		}
	}
</style>
