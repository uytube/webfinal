<%@ page import="servicios.Video"%>
<%@ page import="servicios.Lista"%>
<%@ page import="servicios.Canal"%>
<%@ page import="java.util.List"%>
<jsp:include page="header.jsp"></jsp:include>
<jsp:include page="sideBar.jsp"></jsp:include>
<section class="col-sm-offset-3 col-md-10 col-md-offset-2 main">
	<div class="container-fluid">
		<div class="row">
			<div class="col-md-6 col-xs-12"></div>
			<div class="col-md-3 col-xs-12">
				<select class="form-control" id="alfabeticamente"
					onchange="ordenarAlfabeticamente()">
					<option disabled selected>Ordenar alfabeticamente</option>
					<option>A-Z</option>
					<option>Z-A</option>
				</select>
			</div>
			<div class="col-md-3 col-xs-12">
				<select class="form-control" id="fecha" onchange="ordenarFecha()">
					<option disabled selected>Ordenar por fecha</option>
					<option>Menor-mayor</option>
					<option>Mayor-menor</option>
				</select>
			</div>

		</div>
		<div class="recommended">
			<div class="col-md-12 col-xs-12">
				<h2>Videos</h2>
			</div>

			<div class="recommended-grids">
				<%
					if (request.getAttribute("listaVideos") != null) {
						for (Video v : (Video[]) request.getAttribute("listaVideos")) { 
							%>
							<div class="col-md-3 resent-grid recommended-grid">
								<div class="resent-grid-img recommended-grid-img">
									<%
										String[] idYoutube = v.getUrl().split("/");
									%>
									<a href="ver/<%=idYoutube[idYoutube.length - 1]+"@"+v.getId()%>" class="title"><img
										src="https://img.youtube.com/vi/<%=idYoutube[idYoutube.length - 1]%>/hqdefault.jpg"
										alt="" /></a>
									<div class="time small-time">
										<p><%=v.getDuracion()%></p>
									</div>
									<div class="clck small-clck">
										<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
									</div>
								</div>
								<div
									class="resent-grid-info recommended-grid-info video-info-grid">
									<h5>
										<a href="ver/<%=idYoutube[idYoutube.length - 1]+"@"+v.getId()%>" class="title"><%=v.getNombre()%></a>
									</h5>
									<ul>
										<li>
											<p class="author author-info">
												<a
													href="/web/canal?canal=<%=v.getCanal().getUsuario().getNickname()%>"
													class="author"><%=v.getCanal().getNombre()%></a>
											</p>
										</li>
										<li class="right-list">
											<p class="views views-info"><%=v.getVisitas() %></p>
										</li>
									</ul>
								</div>
							</div>
							<%
							}
					}
				%>
				<div class="clearfix"></div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<h2>Listas</h2>
			</div>
			<div class="col-md-12 col-xs-12">
				<%
					if (request.getAttribute("listaListas") != null) {
						for (Lista l : (Lista[]) request.getAttribute("listaListas")) {
				%>
				<div class="recommended">
					<div class="recommended-grids">
						<div class="recommended-info">
							<a href="/web/listasReproduccion?id_lista=<%=l.getId()%>">
								<h3><%=l.getNombre()%></h3>
							</a>
						</div>
						<%
							if (((Lista) l).getVideos() != null) {
										for (Video v : ((Lista) l).getVideos()) {
						%>
						<div class="col-md-3 resent-grid recommended-grid">
							<div class="resent-grid-img recommended-grid-img">
								<%
									String[] idYoutube = v.getUrl().split("/");
								%>
								<a href="ver?id_video=<%=v.getId()%>"><img
									src="https://img.youtube.com/vi/<%=idYoutube[idYoutube.length - 1]%>/hqdefault.jpg"
									alt="" /></a>
								<div class="time small-time">
									<p><%=v.getDuracion()%></p>
								</div>
								<div class="clck small-clck">
									<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
								</div>
							</div>
							<div
								class="resent-grid-info recommended-grid-info video-info-grid">
								<h5>
									<a href="ver?id_video=<%=v.getId()%>" class="title"><%=v.getNombre()%></a>
								</h5>
								<ul>
									<li><a
										href="/web/canal?canal=<%=v.getCanal().getUsuario().getNickname()%>"
										class="author"><%=v.getCanal().getNombre()%></a></li>
								</ul>

							</div>
						</div>
						<%
							}
									}
								}
						%>
						<div class="clearfix"></div>
					</div>
					<a href="#">Ver lista</a>
				</div>
				<%
					}
				%>
			</div>
		</div>
		<div class="recommended">
			<div class="col-md-12 col-xs-12">
				<h2>Canales</h2>
			</div>

			<div class="recommended-grids">
				<%
					if (request.getAttribute("listaCanales") != null) {
						for (Canal c : (Canal[]) request.getAttribute("listaCanales")) {
				%>
				<div class="col-md-3 resent-grid recommended-grid">
					<div class="resent-grid-img recommended-grid-img">
						<a href="/web/canal?canal=<%=c.getNombre()%>"> <%
 	if (c.getUsuario().getImg().length() != 0) {
 %>
							<div class="profileImg"
								style="background:url(/web/files?image=<%=c.getUsuario().getImg()%>)"></div>
							<%
								} else {
							%> <img class="profileImg"
							src="https://img.youtube.com/vi/hqdefault.jpg" alt="" /></a>
						<%
							}
						%>
						<div class="clck small-clck">
							<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
						</div>
					</div>
					<div class="resent-grid-info recommended-grid-info video-info-grid">
						<h5>
							<a href="/web/canal?canal=<%=c.getUsuario().getNickname()%>" class="title">
								<%=c.getNombre()%>
							</a>
						</h5>
					</div>
				</div>
				<%
					}
					}
				%>
				<div class="clearfix"></div>
			</div>
		</div>
	</div>
</section>

<script>
	function ordenarFecha() {
		var fecha = document.getElementById("fecha").value
		if ('URLSearchParams' in window) {
			var searchParams = new URLSearchParams(window.location.search);
			searchParams.set("fecha", fecha);
			window.location.search = searchParams.toString();
		}
	}
	function ordenarAlfabeticamente() {
		var alfabeticamente = document.getElementById("alfabeticamente").value
		if ('URLSearchParams' in window) {
			var searchParams = new URLSearchParams(window.location.search);
			searchParams.set("alfabeticamente", alfabeticamente);
			window.location.search = searchParams.toString();
		}
	}
</script>
<style>
.profileImg {
	height: 200px;
	background-size: cover;
	background-repeat: no-repeat;
	background-position: center;
}
</style>