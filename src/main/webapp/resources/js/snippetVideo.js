$("#formSnippet")
		.submit(
				function(event) {
					event.preventDefault();

					let url_query_snippet = 'https://www.googleapis.com/youtube/v3/videos?part=snippet%2CcontentDetails%2Cstatistics&id=';
					var id_yt = $("#url").val();
					var id_youtube = youtube_parser(id_yt);
					url_query_snippet += id_youtube;
					var url_key = '&key=AIzaSyCq-jwiFhEd9IuaxshlP66nQ6wHBEKsWvE';
					url_query_snippet += url_key;

					$.getJSON(url_query_snippet, function(data) {

						var data = {
							title : data.items[0].snippet.title,
							duracion : data.items[0].contentDetails.duration,
							views : data.items[0].statistics.viewCount,
							url:  id_youtube,
							privacidad: $("#privacidadVideo").val(),
							categoria: $("#categoria").val(),
							descripcion: $("#descripcion").val()
						}
						
						$.ajax({
							url : "subirVideoYT",
							type : "post",
							data : data,
							success : 
						    function(res) {
								var successUrl = "/web/home";
								 window.location.href = successUrl;
								alert("El video ha sido subido");
							},
						})

					});

				});

// CORTA CUALQUIER TIPO DE URL DE YOUTUBE
function youtube_parser(url) {
	var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
	var match = url.match(regExp);
	return (match && match[7].length == 11) ? match[7] : false;
}
