$("#formModificarListaRepro").submit(function(event) {
	event.preventDefault();

	var data = {
		privacidad : $("#privacidad").val(),
		idLista : $("#idLista").val(),
	}

	$.ajax({
		url : "/web/modificarLista",
		type : "post",
		data : data,
		success : function(res) {
			var successUrl = "/web/listasReproduccion?idLista=";
			successUrl += $("#idLista").val();
			window.location.href = successUrl;
			alert("La lista ha sido modificada correctamente");
		},
	})

});
