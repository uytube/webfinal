<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<link rel="icon" href="https://i.imgur.com/JQ0nuOz.png">
<title>UyTube | Dar cuenta de baja</title>

</head>
<jsp:include page="header.jsp"></jsp:include>
<body class="bajacuenta">

	<jsp:include page="sideBar.jsp"></jsp:include>
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		<div class="col-sm-8 single-left">
			<h1>SUS DATOS SE BORRAR�N, �DESEA CONTINUAR?</h1>
			<div
				style="position: fixed; top: 50%; left: 50%; transform: translate(-25%, -50%);" ">

				<form action="darDeBaja" method="post">
					<a class="user-icon"><button class="boton"
							style="font-size: 20px" type="submit"
							onclick="return confirm('�Est�s segur@ de dar de baja?')">S�,
							dar de baja mi cuenta</button></a> 
				</form>
				<form action="darDeBaja" method="get"><button
							class="boton" style="margin-top:10px">CANCELAR</button></form>
			</div>
		</div>
	</div>
</body>
</html>