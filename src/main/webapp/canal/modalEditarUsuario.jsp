<%@ page import="servicios.Usuario"%>
<%@ page import="servicios.Canal"%>
<div id="modalEditarUsuario" class="modal" role="dialog">
	<div class="modal-dialog">

		<form action="/web/canal" method="post" enctype='multipart/form-data'>
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Editar usuario</h4>
				</div>
				<div class="modal-body">
					<p class="text-danger" id="userexists-email" style="display: none">Correo
						ya en uso</p>
					<p class="text-danger" id="password-check" style="display: none">Passwords
						no coinciden</p>

					<div class="row">
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<input type="text" readonly class="form-control"
									value="<%=((Usuario) request.getAttribute("usuario")).getNickname()%>"
									name="nickname" placeholder="Nickname">
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<input type="text" class="form-control"
									value="<%=((Usuario) request.getAttribute("usuario")).getNombre()%>"
									name="nombre" placeholder="Nombre">
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<input type="text" class="form-control"
									value="<%=((Usuario) request.getAttribute("usuario")).getApellido()%>"
									name="apellido" placeholder="Apellido">
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<input type="email" class="form-control"
									value="<%=((Usuario) request.getAttribute("usuario")).getCorreo()%>"
									name="correo" oninput="checkEmail()" placeholder="correo">
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<%
									java.text.DateFormat df = new java.text.SimpleDateFormat("yyyy-MM-dd");
								%>
								<input type="date" class="form-control"
									value="<%=df.format(((Usuario) request.getAttribute("usuario")).getFnacimiento())%>"
									name="fdate" placeholder="Fecha de nacimiento">
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<input type="password" id="password" oninput="checkPass()"
									class="form-control" name="password"
									placeholder="Password">
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<input type="password" id="password-repeat"
									oninput="checkPass()" class="form-control"
									name="password-check" placeholder="Repetir password">
							</div>
						</div>
						<div class="col-md-6 col-xs-12">
							<div class="form-group">
								<input type="text" class="form-control"
									value="<%=((Canal) request.getAttribute("canal")).getNombre()%>"
									name="canalName" placeholder="Nombre del canal">
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<div class="form-group">
								<textarea class="form-control" name="canalDescription"
									placeholder="Descripcion del canal"><%=((Canal) request.getAttribute("canal")).getDescripcion()%></textarea>
							</div>
						</div>
						<div class="col-md-12 col-md-6">
							<div class="form-group">
								<label> <input type="checkbox"
									value="<%=((Canal) request.getAttribute("canal")).isPrivacidad()%>"
									name="canalPrivacity">Es privado?
								</label>
							</div>
						</div>
						<div class="col-md-12 col-xs-12">
							<span class="btn btn-default btn-file"> Foto de perfil
								(Opcional) <input name="file" type="file">
							</span>
						</div>
						<div class="col-md-12 col-xs-12"></div>
					</div>
					<div class="modal-footer">
						<button class="btn btn-success submit" type="submit">Editar
							usuario</button>

						<button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
					</div>
				</div>

			</div>
		</form>

	</div>
</div>
<script>
	function checkNickname(e) {
		var nickname = document.getElementById("nickname").value
		fetch(`/web/findByNickname?nickname=${nickname}`).then(
				function(response) {
					return response.json();
				}).then(function(myJson) {
			console.log(myJson);
			if (myJson == true) {
				document.getElementById("submit").disabled = true
				document.getElementById("userexists").style.display = 'block'
			} else {
				document.getElementById("submit").disabled = false
				document.getElementById("userexists").style.display = 'none'
			}
		});

	}
	function checkEmail(e) {
		var correo = document.getElementById("correo").value
		fetch(`/web/findByMail?correo=${correo}`)
				.then(function(response) {
					return response.json();
				})
				.then(
						function(myJson) {
							console.log(myJson);
							if (myJson == true) {
								document.getElementById("submit").disabled = true
								document.getElementById("userexists-email").style.display = 'block'
							} else {
								document.getElementById("submit").disabled = false
								document.getElementById("userexists-email").style.display = 'none'
							}
						});

	}
	function checkPass() {
		var password = document.getElementById('password');
		var passwordRepeat = document.getElementById('password-repeat');
		if (password.value != passwordRepeat.value) {
			document.getElementById("password-check").style.display = 'block'
			document.getElementById("submit").disabled = true
		} else {
			document.getElementById("password-check").style.display = 'none'
			document.getElementById("submit").disabled = false
		}
	}
</script>

<style>
.modal-backdrop {
	z-index: 1 !important;
}

.my-mfp-zoom-in.mfp-ready.mfp-bg {
	display: none;
}
</style>