<%@ page import="servicios.Usuario"%>
<%@ page import="servicios.Canal"%>
<%@ page import="servicios.Lista"%>
<%@ page import="servicios.Historial"%>
<%@ page import="java.util.List"%>
<%@ page import="servicios.Video"%>
<%
	java.text.DateFormat df = new java.text.SimpleDateFormat("dd/MM/yyyy");
%>

<jsp:include page="../header.jsp"></jsp:include>
<body>
	<jsp:include page="../sideBar.jsp"></jsp:include>
	<section class="col-sm-offset-3 col-md-10 col-md-offset-2 main">
		<div class="row header-canal">
			<div class="col-md-3 col-xs-12 flex-center">
				<img class="profile"
					src="data:image/jpeg;base64,<%=((Usuario) request.getAttribute("usuario")).getImg()%>">
			</div>
			<div class="col-md-9 col-xs-12">
				<div class="row" style="display: flex; align-items: center">
					<div class="col-md-3 col-xs-12">
						<h2><%=((Usuario) request.getAttribute("usuario")).getNickname()%></h2>
						<p><%=((Canal) request.getAttribute("canal")).getDescripcion()%></p>
						<p>
							<%=((Usuario) request.getAttribute("usuario")).getNombre()%>&nbsp;
							<%=((Usuario) request.getAttribute("usuario")).getApellido()%>
						</p>
					</div>
					<div class="col-md-9">
						<%
							if (request.getSession().getAttribute("usuarioLogueado") != null) {
						%>
						<%
							if (((String) request.getAttribute("duenioCanal")).equals("true")) {
						%>
						<button class="btn btn-success float-right" data-toggle="modal"
							data-target="#modalEditarUsuario">Editar usuario</button>
						<jsp:include page="modalEditarUsuario.jsp"></jsp:include>

						<%
							} else {
						%>
						<div class="user signin float-right">
							<form id="form_subscripcion" method="post" action="seguir">
								<input type="hidden"
									value="<%=((Usuario) request.getAttribute("usuario")).getNickname()%>"
									name="nickname" id="nickname"> <input type="hidden"
									value="<%=((Canal) request.getAttribute("canal")).getNombre()%>"
									name="canal_nombre" id="canal_nombre">
								<%
									if (request.getAttribute("siguiendo").equals("false")) {
								%>
								<input type="hidden" value="true" name="subscribe"
									id="subscribe">
								<button type="submit" id="subscripcion">Subscribirse</button>
								<%
									} else {
								%>
								<input type="hidden" value="false" name="subscribe"
									id="subscribe">
								<button type="submit" class="btn btn-outline" id="subscripcion">Dejar
									de seguir</button>
								<%
									}
								%>

							</form>
						</div>
						<%
							}
						%>
						<%
							}
						%>


					</div>
				</div>
			</div>
		</div>
		<div class="container-fluid">
			<br>
			<ul class="nav nav-tabs">
				<li class="active"><a data-toggle="tab" href="#videos">Videos</a></li>
				<li><a data-toggle="tab" href="#listas">Listas</a></li>
				<li><a data-toggle="tab" href="#seguidores">Seguidores</a></li>
				<li><a data-toggle="tab" href="#historial">Historial</a></li>
			</ul>

			<div class="tab-content">
				<div id="videos" class="tab-pane fade in active">
					<div class="recommended">
						<div class="recommended-grids">
							<% if (request.getAttribute("listaVideos")!=null){ %>
							<%
								for (Video v : (Video[]) request.getAttribute("listaVideos")) {
							%>
							<%
								if (v.isEs_publico() == true || ((String) request.getAttribute("duenioCanal")).equals("true")) {
							%>
							<div class="col-md-3 resent-grid recommended-grid">
								<div class="resent-grid-img recommended-grid-img">
									<%
										String[] idYoutube = v.getUrl().split("/");
									%>
									<a href="ver/<%=idYoutube[idYoutube.length - 1]+"@"+v.getId()%>"><img
										src="https://img.youtube.com/vi/<%=idYoutube[idYoutube.length - 1]%>/hqdefault.jpg"
										alt="" /></a>
									<div class="time small-time">
										<p><%=v.getDuracion() %></p>
									</div>
									<div class="clck small-clck">
										<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
									</div>
								</div>
								<div
									class="resent-grid-info recommended-grid-info video-info-grid">
									<h5>
										<a href="ver/<%=idYoutube[idYoutube.length - 1]+"@"+v.getId()%>" class="title"><%=v.getNombre()%></a>
									</h5>
									<ul>
										<%
											if (request.getSession().getAttribute("usuarioLogueado") != null) {
										%>
										<%
											if (((String) request.getAttribute("duenioCanal")).equals("true")) {
										%>
										<li><a href="/web/modificarVideo?id_video=<%=v.getId()%>"
											class="author">Modificar video</a></li>
										<%
											}
										%>
										<%
											}
										%>
									</ul>
								</div>
							</div>
							<%
								}
								}
								}
							%>
							<div class="clearfix"></div>
						</div>
					</div>

				</div>
				<div id="listas" class="tab-pane fade">
					<div class="row">
						<div class="col-md-12 col-xs-12">
							<%
								if(request.getAttribute("listas")!=null){
								for (Lista l : (Lista[]) request.getAttribute("listas")) {
							%>
							<%
								if (l.getPrivado() == false || ((String) request.getAttribute("duenioCanal")).equals("true")) {
							%>
							<div class="recommended">
								<div class="recommended-grids">
									<div class="recommended-info">
										<a href="/web/listasReproduccion?id_lista=<%=l.getId()%>">
											<h3><%=l.getNombre()%></h3>
										</a>
									</div>
									<%if(((Lista) l).getVideos()!=null){
										for (Video v : ((Lista) l).getVideos()) {
									%>
									<div class="col-md-3 resent-grid recommended-grid">
										<div class="resent-grid-img recommended-grid-img">
											<%
												String[] idYoutube = v.getUrl().split("/");
											%>
											<a href="ver?id_video=<%=v.getId()%>"><img
												src="https://img.youtube.com/vi/<%=idYoutube[idYoutube.length - 1]%>/hqdefault.jpg"
												alt="" /></a>
											<div class="time small-time">
												<p><%=v.getDuracion()%></p>
											</div>
											<div class="clck small-clck">
												<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
											</div>
										</div>
										<div
											class="resent-grid-info recommended-grid-info video-info-grid">
											<h5>
												<a href="ver?id_video=<%=v.getId()%>" class="title"><%=v.getNombre()%></a>
											</h5>
											<ul>
												<li><a
													href="/web/canal?canal=<%=v.getCanal().getUsuario().getNickname()%>"
													class="author"><%=v.getCanal().getNombre()%></a></li>
											</ul>

										</div>
									</div>
									<%
										}
										}
									%>
									<div class="clearfix"></div>
								</div>
								<div style="display: flex;">
									<%
										if (request.getSession().getAttribute("usuarioLogueado") != null) {
									%>
									<%
										if (((String) request.getAttribute("duenioCanal")).equals("true")) {
									%>
									<div>
										<form class="uploadvideo" action="modificarLista"
											method="post">
											<input class="btn btn-danger" type="submit" name="privacity"
												value="<%=(l.getPrivado() == false) ? "Publico" : "Privado"%>">
											<input type="hidden" name="idListaRepro"
												value="<%=l.getId()%>">
										</form>
									</div>
									<%
										}
									%>
									<%
										}
									%>
									<div style="margin-left: 5px;">
										<a href="/web/listasReproduccion?idLista=<%=l.getId()%>">
											<button class="btn btn-info">Ver lista</button>
										</a>

									</div>
									<div class="col-md-4 col-xs-12"></div>
								</div>

							</div>
							<%
								}
							%>
							<%
								}
								}
							%>
						</div>
					</div>
				</div>
				<div id="seguidores" class="tab-pane fade">
					<div class="recommended">
						<div class="recommended-grids">
							<div class="recommended-info">

								<div class="row">
									<div class="col-md-6 col-xs-12">
										<ul class="list-group">
											<li class="list-group-item active">Seguidos</li>
											<%
												if (request.getAttribute("seguidos") != null) {
													for (Canal c : (Canal[]) request.getAttribute("seguidos")) {
											%>
											<li class="list-group-item"><%=c.getNombre()%></li>
											<%
												}

												}
											%>

											<li></li>
										</ul>
									</div>
									<div class="col-md-6 col-xs-12">
										<ul class="list-group">
											<li class="list-group-item active">Seguidores</li>

											<%
												if (request.getAttribute("seguidores") != null) {
													for (Usuario u : (Usuario[]) request.getAttribute("seguidores")) {
											%>
											<li class="list-group-item"><%=u.getNombre()%></li>
											<%
												}
												}
											%>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div id="historial" class="tab-pane fade">
					<div class="recommended">
						<div class="recommended-grids">
							<div class="recommended-info">

								<div class="row">
									<div class="col-md-12 col-xs-12">
										<ul class="list-group">
											<li class="list-group-item active">Historial</li>
											<%
												if (request.getAttribute("historial") != null) {
													for (Historial h : (Historial[]) request.getAttribute("historial")) {
											%>
											<li class="list-group-item"><%= h.getVideo().getNombre()+"-"+
											h.getFecha().getDate()+"/"+h.getFecha().getMonth()+1+"/"+h.getFecha().getYear()%></li>
											<%
												}

												}
											%>

											<li></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="/web/resources/js/bootstrap.min.js"></script>
	<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
</body>

</html>
<style>
.header-canal {
	background: #8080805e;
	min-height: 100px;
	display: flex;
	align-items: center;
}

.float-right {
	float: right;
	margin-right: 5%;
}

.profile {
	width: 80px;
	height: 80px;
	border-radius: 50px;
}

.flex-center {
	display: flex;
	align-items: center;
	justify-content: center;
}
</style>
<script src="/web/resources/js/ajax.js"></script>