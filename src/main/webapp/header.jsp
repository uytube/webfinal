<%@ page import="servicios.Lista"%>
<%@ page import="java.util.List"%>
<%@ page import="servicios.Categoria"%>
<%@ page import="servicios.Usuario"%>
<!DOCTYPE HTML>
<html>
<head>
<title>UyTube | Home</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords"
	content="My Play Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript">
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	 addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } 






























</script>
<!-- bootstrap -->
<link href="/web/resources/css/bootstrap.min.css" rel='stylesheet'
	type='text/css' media="all" />
<!-- //bootstrap -->
<link href="/web/resources/css/dashboard.css" rel="stylesheet">
<!-- Custom Theme files -->
<link href="/web/resources/css/style.css" rel='stylesheet'
	type='text/css' media="all" />
<script src="https://code.jquery.com/jquery-1.12.4.min.js"
	integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ="
	crossorigin="anonymous"></script>
<script src="http://code.jquery.com/jquery-migrate-1.4.1.js"></script>
<!--start-smoth-scrolling-->
<!-- fonts -->
<link
	href='//fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800'
	rel='stylesheet' type='text/css'>
<link href='//fonts.googleapis.com/css?family=Poiret+One'
	rel='stylesheet' type='text/css'>
<!-- //fonts -->

</head>


<header>
	<nav class="navbar navbar-inverse navbar-fixed-top">
		<div class="container-fluid header-mobile">
			<div class="row">
				<div class="col-xs-3 navbar-header" style="padding: 0">
					<button type="button" class="navbar-toggle" data-toggle="collapse"
						data-target="#myNavbar">
						<span class="icon-bar"></span> <span class="icon-bar"></span> <span
							class="icon-bar"></span>
					</button>
				</div>
				<div class="col-xs-6" style="padding: 0">
					<h4>Bienvenidos a UyTube!</h4>
				</div>
				<div class="col-xs-3" style="padding: 0">
					<img src="/web/resources/images/logo.png" alt=""
						style="max-height: 60px; margin-top: -5px;" />
				</div>
			</div>
			<div class="collapse navbar-collapse" id="myNavbar"
				style="height: 0; overflow: auto;">
				<ul class="nav navbar-nav">
					<li><a href="/web/home">Home</a></li>
					<li><a href="#">Ver videos</a></li>
					<%		
						if (request.getSession().getAttribute("usuarioLogueado") != null) {
					%>
					<li><a href="/web/listascelular.jsp">Ver listas</a></li>
					<li><a href="/web/subirVideoYT">Subir video</a></li>
					<li><a href="/web/canal?canal=<%=((Usuario) request.getSession().getAttribute("usuarioLogueado")).getNickname()%>">Mi canal</a></li>
					<jsp:include page="modalCrearLista.jsp"></jsp:include>

					<li><a class="play-icon popup-with-zoom-anim"
						data-toggle="modal" data-target="#ModalCrearLista">+ Crear
							lista</a></li>
					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">Listas <span class="caret"></span></a>
						<ul class="dropdown-menu">
						<%
						for (Lista l : (Lista[]) request.getSession().getAttribute("listas")) {
						%>
						<li><a href="listasReproduccion?idLista=<%=l.getId()%>"><%=l.getNombre()%></a></li>
						<%
							}
						%>
						</ul>
						<li><a href="/web/logout">Desconectarse</a></li>
					</li>
					<%
						} else {
					%>
					<li><a href="/web/mobile-login.jsp">Iniciar sesion</a></li>
					<%
						}
					%>
					


					<li class="dropdown"><a href="#" class="dropdown-toggle"
						data-toggle="dropdown" role="button" aria-haspopup="true"
						aria-expanded="false">Categorias <span class="caret"></span></a>
						<ul class="dropdown-menu">
							<%
								if (request.getSession().getAttribute("categorias") != null) {
							%>
							<%
								for (Categoria c : (Categoria[]) request.getSession().getAttribute("categorias")) {
							%>
							<li><a href="categorias?nombreCategoria=<%=c.getNombre()%>"><%=c.getNombre()%></a></li>
							<%
								}
							%>
							<%
								}
							%>
						</ul></li>

				</ul>
			</div>
			<!-- /.navbar-collapse -->

		</div>
		<div class="container-fluid header-pc">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle collapsed"
					data-toggle="collapse" data-target="#navbar" aria-expanded="false"
					aria-controls="navbar">
					<span class="sr-only">Toggle navigation</span> <span
						class="icon-bar"></span> <span class="icon-bar"></span> <span
						class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/web/home"><h1>
						<img src="/web/resources/images/logo.png" alt=""
							style="max-height: 60px; margin-top: -5px;" />
					</h1></a>
			</div>
			<div class="top-search">
				<form class="navbar-form navbar-right" action="/web/search"
					method="get">
					<input type="text" class="form-control" name="q"
						placeholder="Search..."> <input type="hidden"
						name="alfabeticamente"> <input type="hidden" name="fecha">
					<input type="submit" value=" ">
				</form>
			</div>
			<div class="header-top-right">
				<%
					if (request.getSession().getAttribute("usuarioLogueado") != null) {
				%>
				<a
					href="/web/canal?canal=<%=((Usuario) request.getSession().getAttribute("usuarioLogueado")).getNickname()%>">
					<div class="file" style="margin-top: 0;">
						<%
							if (((Usuario) request.getSession().getAttribute("usuarioLogueado")).getImg() == "") {
						%>
						<img class="profile-img" src="/web/resources/images/profile.png"
							alt="perfil">
						<%
							} else {
						%>
						<img class="profile-img"
							src="data:image/jpeg;base64,<%=((Usuario) request.getSession().getAttribute("usuarioLogueado")).getImg()%>"
							alt="perfil">
						<%
							}
						%>
					</div>
					<div class="file">
						<p id="usuarioLogueado"><%=((Usuario) request.getSession().getAttribute("usuarioLogueado")).getNickname()%></p>
					</div>
				</a>
				<div class="signin">
					<a href="logout" class="play-icon">Cerrar sesion</a>


				</div>
				<%
					} else {
				%>
				<div class="signin">
					<a data-toggle="modal" href="#" data-target="#modalRegistro"
						class="play-icon popup-with-zoom-anim">Registro</a>
					<!-- pop-up-box -->
					<script type="text/javascript"
						src="/web/resources/js/modernizr.custom.min.js"></script>
					<link href="/web/resources/css/popuo-box.css" rel="stylesheet"
						type="text/css" media="all" />
					<script src="/web/resources/js/jquery.magnific-popup.js"
						type="text/javascript"></script>
					<!--//pop-up-box -->
					<script>
						$(document).ready(function() {
							$('.popup-with-zoom-anim').magnificPopup({
								type : 'inline',
								fixedContentPos : false,
								fixedBgPos : true,
								overflowY : 'auto',
								closeBtnInside : true,
								preloader : false,
								midClick : true,
								removalDelay : 300,
								mainClass : 'my-mfp-zoom-in'
							});

						});
					</script>
				</div>

				<div id="modalRegistro" class="modal" role="dialog">
					<div class="modal-dialog">
						<form method="post" action="/web/signup"
							enctype='multipart/form-data'>

							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Registrarse</h4>
								</div>
								<div class="modal-body">
									<p class="text-danger" id="userexists" style="display: none">Nickname
										ya en uso</p>
									<p class="text-danger" id="userexists-email"
										style="display: none">Correo ya en uso</p>
									<p class="text-danger" id="password-check"
										style="display: none">Passwords no coinciden</p>
									<div class="form-group">
										<input type="text" id="nickname" required class="form-control"
											oninput="checkNickname()" name="nickname"
											placeholder="Nickname">
									</div>
									<div class="form-group">
										<input type="text" class="form-control" required name="nombre"
											placeholder="Nombre">
									</div>
									<div class="form-group">
										<input type="text" class="form-control" required
											name="apellido" placeholder="Apellido">
									</div>
									<div class="form-group">
										<input type="email" class="form-control" required id="correo"
											oninput="checkEmail()" name="correo" placeholder="correo">
									</div>
									<div class="form-group">
										<input type="date" class="form-control" required name="fdate"
											placeholder="Fecha de nacimiento">
									</div>
									<div class="form-group">
										<input type="password" id="password" oninput="checkPass()"
											class="form-control" required name="password"
											placeholder="Password">
									</div>
									<div class="form-group">
										<input type="password" id="password-repeat"
											oninput="checkPass()" class="form-control" required
											name="password-check" placeholder="Repetir password">
									</div>
									<div class="form-group">
										<input type="text" class="form-control" required
											name="canalName" placeholder="Nombre del canal">
									</div>
									<div class="form-group">
										<textarea class="form-control" required
											name="canalDescription" placeholder="Descripcion del canal"></textarea>
									</div>
									<div class="form-group">
										<label> <input type="checkbox" name="canalPrivacity">Es
											privado?
										</label>
									</div>
									<div class="form-group">
										<span class="btn btn-default btn-file"> Foto de perfil
											(Opcional) <input name="file" type="file">
										</span>
									</div>
									<div class="modal-footer">
										<button type="submit" id="submit" class="btn btn-success">Registrarse</button>
										<button type="button" class="btn btn-default"
											data-dismiss="modal">Cerrar</button>
									</div>

								</div>
						</form>

					</div>
				</div>
			</div>
			<script>
				function checkNickname(e) {
					var nickname = document.getElementById("nickname").value
					fetch(`/web/findByNickname?nickname=${nickname}`)
							.then(function(response) {
								return response.json();
							})
							.then(
									function(myJson) {
										console.log(myJson);
										if (myJson == true) {
											document.getElementById("submit").disabled = true
											document
													.getElementById("userexists").style.display = 'block'
										} else {
											document.getElementById("submit").disabled = false
											document
													.getElementById("userexists").style.display = 'none'
										}
									});

				}
				function checkEmail(e) {
					var correo = document.getElementById("correo").value
					fetch(`/web/findByMail?correo=${correo}`)
							.then(function(response) {
								return response.json();
							})
							.then(
									function(myJson) {
										console.log(myJson);
										if (myJson == true) {
											document.getElementById("submit").disabled = true
											document
													.getElementById("userexists-email").style.display = 'block'
										} else {
											document.getElementById("submit").disabled = false
											document
													.getElementById("userexists-email").style.display = 'none'
										}
									});

				}
				function checkPass() {
					var password = document.getElementById('password');
					var passwordRepeat = document
							.getElementById('password-repeat');
					if (password.value != passwordRepeat.value) {
						document.getElementById("password-check").style.display = 'block'
						document.getElementById("submit").disabled = true
					} else {
						document.getElementById("password-check").style.display = 'none'
						document.getElementById("submit").disabled = false
					}
				}
			</script>
			<div id="modalLogin" class="modal" role="dialog">
				<form action="/web/login" method="post">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<button type="button" class="close" data-dismiss="modal">&times;</button>
								<h4 class="modal-title">Registrarse</h4>
							</div>
							<div class="modal-body">

								<div class="form-group">
									<input type="text" class="form-control" placeholder="Usuario"
										required="required" name="user" />
								</div>
								<div class="form-group">
									<input name="password" type="password" placeholder="Password"
										required="required" class="form-control" autocomplete="off" />
								</div>
							</div>
							<div class="modal-footer">
								<button type="submit" id="submit" class="btn btn-success">Iniciar
									sesion</button>
								<button type="button" class="btn btn-default"
									data-dismiss="modal">Cerrar</button>
							</div>
						</div>
					</div>
				</form>

			</div>
			<div class="signin">
				<a data-target="#modalLogin" href="#" data-toggle="modal"
					class="play-icon popup-with-zoom-anim">Iniciar sesion</a>
			</div>

			<%
				}
			%>
			<!-- 			<div class="clearfix"></div> -->
		</div>
		<!-- 		<div class="clearfix"></div> -->
	</nav>
</header>
<style>
.profile-img {
	width: 50px;
	height: 50px;
	border-radius: 50px;
}

.sidebar-celular {
	display: none;
}
</style>