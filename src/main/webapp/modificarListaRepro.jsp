<%@ page import="servicios.Lista"%>
<%@ page import="servicios.Video"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<meta charset="ISO-8859-1">
<title>UyTube | Modificar video</title>
</head>
<jsp:include page="header.jsp"></jsp:include>
<body class="uploadvideo">
	<jsp:include page="sideBar.jsp"></jsp:include>
	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		<div class="container-fluid">
			<h2>Modificar Lista de Reproduccion</h2>
			<form id="formModificarListaRepro">
				<script src="/web/resources/js/modificarListaRepro.js"></script>
				<%
					Lista l = (Lista) request.getAttribute("listaModi");
				%>

				<input type="hidden" id="idLista" name="id" value="<%=l.getId()%>">

				<div class="row">
					<div class="col-md-12 col-xs-12">
						<div class="form-group">
							<label style="font-size: 15px">Nombre</label> <input id="nombre"
								class="form-control" type="text" style="width: 40%"
								name="nombre" value="<%=l.getNombre()%>">
						</div>
					</div>
					<div class="col-md-12 col-xs-12">
						<div class="form-group">
							<label style="font-size: 15px">Privacidad </label><br> <select
								style="font-size: 15px; width: 100px; padding: 5px 0px 5px 10px; vertical-align: middle; border-collapse: collapse;"
								id="privacidad" name="privacidad">
								<option value="false">Publica</option>
								<option value="true">Privada</option>
							</select>

						</div>
					</div>
					<div class="col-md-12 col-xs-12">
						<div class="form-group">
							<input id="formModificarListaRepro" type="submit"
								class="btn btn-success"></input>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>
</body>

</html>
