<%@ page import="servicios.Lista"%>
<%@ page import="java.util.List"%>
<%@ page import="servicios.Categoria"%>
<%@ page import="servicios.Usuario"%>


<!-- SIDEBAR DE PC -->
<div class="col-sm-3 col-md-2 sidebar">
	<div class="top-navigation">
		<div class="t-menu">MENU</div>
		<div class="t-img">
			<img src="/web/resources/images/lines.png" alt="" />
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="drop-navigation drop-navigation">
		<ul class="nav nav-sidebar">
			<li class="active"><a href="home" class="home-icon"><span
					class="glyphicon glyphicon-home" aria-hidden="true"></span>Home</a></li>

			<li><a href="/web/home" class="user-icon"><span
					class="glyphicon glyphicon-home glyphicon-blackboard"
					aria-hidden="true"></span>Videos</a></li>
			<%
				if (request.getSession().getAttribute("usuarioLogueado") != null) {
			%>
			<ul class="">
				<li><a href="subirVideoYT">Subir video</a></li>
			</ul>
			<%
				}
			%>
			<li><a href="#" class="user-icon menu4"><span
					class="glyphicon glyphicon-home glyphicon-blackboard"
					aria-hidden="true"></span>Listas <span
					class="glyphicon glyphicon-menu-down" aria-hidden="true"></span></a></li>
			<ul class="cl-effect-1">
				<%
					if (request.getSession().getAttribute("usuarioLogueado") != null) {
				%>
				<jsp:include page="modalCrearLista.jsp"></jsp:include>

				<li><a class="play-icon popup-with-zoom-anim"
					data-toggle="modal" data-target="#ModalCrearLista">+ Crear
						lista</a></li>


				<%
					for (Lista l : (Lista[]) request.getSession().getAttribute("listas")) {
				%>
				<li><a href="/web/listasReproduccion?idLista=<%=l.getId()%>"><%=l.getNombre()%></a></li>
				<%
					}
				%>
				<%
					} else {
				%><li><a data-target="#modalLogin" href="#" data-toggle="modal"
					class="play-icon popup-with-zoom-anim">Iniciar sesion</a></li>
				<%
					}
				%>
			</ul>
			<jsp:include page="modalCrearLista.jsp"></jsp:include>
			<!-- script-for-menu -->
			<script>
				$("li a.menu4").click(function() {
					$("ul.cl-effect-1").slideToggle(300, function() {
						// Animation complete.
					});
				});
			</script>
			<!-- script-for-menu -->

			<li><a href="#" class="user-icon menu3"><span
					class="glyphicon glyphicon-home glyphicon-blackboard"
					aria-hidden="true"></span>Categorias <span
					class="glyphicon glyphicon-menu-down" aria-hidden="true"></span></a></li>
			<ul class="cl-effect-2">
				<%
					if (request.getSession().getAttribute("categorias") != null) {
				%>
				<%
					for (Categoria c : (Categoria[]) request.getSession().getAttribute("categorias")) {
				%>
				<li><a href="/web/categorias?nombreCategoria=<%=c.getNombre()%>"><%=c.getNombre()%></a></li>
				<%
					}
				%>
				<%
					}
				%>
			</ul>
			<%
				if (request.getSession().getAttribute("usuarioLogueado") != null) {
			%>
			<li><a href="/web/darCuentaBaja.jsp" class="user-icon"
				style="color: #CF7095; font-style: italic"><span
					class="glyphicon glyphicon-home glyphicon-blackboard"
					aria-hidden="true"></span>Dar cuenta de baja</a></li>
			<%
				}
			%>

			<script>
				$("li a.menu3").click(function() {
					$("ul.cl-effect-2").slideToggle(300, function() {
						// Animation complete.
					});
				});
			</script>
		</ul>
	</div>
</div>

<!-- SIDEBAR CELULAR -->
<div class="col-sm-3 col-md-2 sidebar-celular">

	<div class="top-navigation">
		<div class="t-menu">MENU</div>
		<div class="t-img">
			<img src="/web/resources/images/lines.png" alt="" />
		</div>
		<div class="clearfix"></div>
	</div>

	<div class="drop-navigation drop-navigation">
		<ul class="nav nav-sidebar">
			<li class="active"><a href="home" class="home-icon"><span
					class="glyphicon glyphicon-home" aria-hidden="true"></span>Ver
					videos</a></li>
			<%
				if (request.getSession().getAttribute("usuarioLogueado") != null) {
			%>
			<li><a href="listascelular.jsp"><span
					class="glyphicon glyphicon-home glyphicon-blackboard"
					aria-hidden="true"></span>Listas de Reproducci�n</a></li>
			<li><a href="logout" class="user-icon"><span
					class="glyphicon glyphicon-home" aria-hidden="true"></span>Cerrar
					sesi�n</a></li>

			<%
				} else {
			%>
			<li><div id="modalLogin" class="modal" role="dialog">
					<form action="/web/login" method="post">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Registrarse</h4>
								</div>
								<div class="modal-body">

									<div class="form-group">
										<input type="text" class="form-control" placeholder="Usuario"
											required="required" name="user" />
									</div>
									<div class="form-group">
										<input name="password" type="password" placeholder="Password"
											required="required" class="form-control" autocomplete="off" />
									</div>
								</div>
								<div class="modal-footer">
									<button type="submit" id="submit" class="btn btn-success">Iniciar
										sesion</button>
									<button type="button" class="btn btn-default"
										data-dismiss="modal">Cerrar</button>
								</div>
							</div>
						</div>
					</form>

				</div> <a class="user-icon menu1" data-target="#modalLogin" href="#"
				data-toggle="modal"><span
					class="glyphicon glyphicon-home glyphicon-blackboard"
					aria-hidden="true"></span>Iniciar sesion</a></li>

			<!-- 					TERMINA EL LOGGIN  -->
			<!-- 					EMPIEZA EL REGISTRO -->

			<li><a a class="user-icon menu1" data-toggle="modal" href="#"
				data-target="#modalRegistro"><span
					class="glyphicon glyphicon-home glyphicon-blackboard"
					aria-hidden="true"></span>Registrarse</a> <!-- pop-up-box --> <script
					type="text/javascript"
					src="/web/resources/js/modernizr.custom.min.js"></script> <!-- 				<link href="/web/resources/css/popuo-box.css" rel="stylesheet" -->
				<!-- 					type="text/css" media="all" /> --> <script>
					src = "/web/resources/js/jquery.magnific-popup.js"
					type = "text/javascript" >
				</script> <!--//pop-up-box --> <script>
						$(document).ready(
								function() {
									$('.popup-with-zoom-anim').magnificPopup({
										type : 'inline',
										fixedContentPos : false,
										fixedBgPos : true,
										overflowY : 'auto',
										closeBtnInside : true,
										preloader : false,
										midClick : true,
										removalDelay : 300,
										mainClass : 'my-mfp-zoom-in'
									});

								});
					</script>
				<div id="modalRegistro" class="modal" role="dialog">
					<div class="modal-dialog">
						<form method="post" action="/web/signup"
							enctype='multipart/form-data'>

							<!-- Modal content-->
							<div class="modal-content">
								<div class="modal-header">
									<button type="button" class="close" data-dismiss="modal">&times;</button>
									<h4 class="modal-title">Registrarse</h4>
								</div>
								<div class="modal-body">
									<p class="text-danger" id="userexists" style="display: none">Nickname
										ya en uso</p>
									<p class="text-danger" id="userexists-email"
										style="display: none">Correo ya en uso</p>
									<p class="text-danger" id="password-check"
										style="display: none">Passwords no coinciden</p>
									<div class="form-group">
										<input type="text" id="nickname" required class="form-control"
											oninput="checkNickname()" name="nickname"
											placeholder="Nickname">
									</div>
									<div class="form-group">
										<input type="text" class="form-control" required name="nombre"
											placeholder="Nombre">
									</div>
									<div class="form-group">
										<input type="text" class="form-control" required
											name="apellido" placeholder="Apellido">
									</div>
									<div class="form-group">
										<input type="email" class="form-control" required id="correo"
											oninput="checkEmail()" name="correo" placeholder="correo">
									</div>
									<div class="form-group">
										<input type="date" class="form-control" required name="fdate"
											placeholder="Fecha de nacimiento">
									</div>
									<div class="form-group">
										<input type="password" id="password" oninput="checkPass()"
											class="form-control" required name="password"
											placeholder="Password">
									</div>
									<div class="form-group">
										<input type="password" id="password-repeat"
											oninput="checkPass()" class="form-control" required
											name="password-check" placeholder="Repetir password">
									</div>
									<div class="form-group">
										<input type="text" class="form-control" required
											name="canalName" placeholder="Nombre del canal">
									</div>
									<div class="form-group">
										<textarea class="form-control" required
											name="canalDescription" placeholder="Descripcion del canal"></textarea>
									</div>
									<div class="form-group">
										<label> <input type="checkbox" name="canalPrivacity">Es
											privado?
										</label>
									</div>
									<div class="form-group">
										<span class="btn btn-default btn-file"> Foto de perfil
											(Opcional) <input name="file" type="file">
										</span>
									</div>
									<div class="modal-footer">
										<button type="submit" id="submit" class="btn btn-success">Registrarse</button>
										<button type="button" class="btn btn-default"
											data-dismiss="modal">Cerrar</button>
									</div>

								</div>
							</div>
						</form>
					</div>
				</div></li>
	</div>
</div>
<script>
	function checkNickname(e) {
		var nickname = document.getElementById("nickname").value
		fetch(`/web/findByNickname?nickname=${nickname}`).then(
				function(response) {
					return response.json();
				}).then(function(myJson) {
			console.log(myJson);
			if (myJson == true) {
				document.getElementById("submit").disabled = true
				document.getElementById("userexists").style.display = 'block'
			} else {
				document.getElementById("submit").disabled = false
				document.getElementById("userexists").style.display = 'none'
			}
		});

	}
	function checkEmail(e) {
		var correo = document.getElementById("correo").value
		fetch(`/web/findByMail?correo=${correo}`)
				.then(function(response) {
					return response.json();
				})
				.then(
						function(myJson) {
							console.log(myJson);
							if (myJson == true) {
								document.getElementById("submit").disabled = true
								document.getElementById("userexists-email").style.display = 'block'
							} else {
								document.getElementById("submit").disabled = false
								document.getElementById("userexists-email").style.display = 'none'
							}
						});

	}
	function checkPass() {
		var password = document.getElementById('password');
		var passwordRepeat = document.getElementById('password-repeat');
		if (password.value != passwordRepeat.value) {
			document.getElementById("password-check").style.display = 'block'
			document.getElementById("submit").disabled = true
		} else {
			document.getElementById("password-check").style.display = 'none'
			document.getElementById("submit").disabled = false
		}
	}
</script>


<%
	}
%>

<jsp:include page="modalCrearLista.jsp"></jsp:include>
<!-- script-for-menu -->
<script>
	$("li a.menu1").click(function() {
		$("ul.cl-effect-1").slideToggle(300, function() {
			// Animation complete.
		});
	});
</script>
<!-- script-for-menu -->
<script>
	$("li a.menu2").click(function() {
		$("ul.cl-effect-2").slideToggle(300, function() {
			// Animation complete.
		});
	});
</script>

</ul>
</div>
</div>
<script>
	$(".top-navigation").click(function() {
		$(".drop-navigation").slideToggle(300, function() {
			// Animation complete.
		});
	});
</script>
