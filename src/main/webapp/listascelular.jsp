<%@ page import="java.util.List"%>
<%@ page import="servicios.Lista"%>
<!-- link href="/web/resources/css/csslistas.css" rel="stylesheet type="text/css"-->
<jsp:include page="header.jsp"></jsp:include>
<body class="listascelular">
	<jsp:include page="sideBar.jsp"></jsp:include>
	</br></br>
	<h2>&nbsp;&nbsp;Listas</h2>

	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		<div class="container-fluid">
			<table class="listascelular">
				<tr style="font-weight:bold;">
					<td data-title="Name">Nombre</td>
					<td data-title="espacio">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
					<td data-title="Status"></td>
					<td data-title="Status">Estado</td>

				</tr>
				<%
					for (Lista l : (Lista[]) request.getSession().getAttribute("listas")) {
				%>
				
				<tr
					style="padding-bottom: 2px; font-weight: @table-header-font-weight; color: @table-font-color; vertical-align: bottom; border-bottom: 2px solid rgba(0, 0, 0, .12);">
					<td data-title="Name"><a href="listasReproduccion?idLista=<%=l.getId()%>"><%=l.getNombre()%></a></td>
					<td data-title="espacio">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>

					<td data-title="Status"></td>
					<%
						if (l.getPrivado() && (!l.getTipo())) {
					%>

					<td data-title="Status" style="background-color: red; color: #FFF;">&nbsp;
						Privado</td>
					<%
						} else if (!l.getPrivado() && (!l.getTipo())) {
					%>
					<td data-title="Status"
						style="background-color: green; color: #FFF;">&nbsp; P�blico</td>
					<%
						} else {
					%>
					<td data-title="Status"
						style="background-color: blue; color: #FFF;">&nbsp; Default</td>
					<%
						}
					%>
				</tr>
				<%
					}
				%>
			</table>
		</div>
	</div>
	<div class="clearfix"></div>
	<!-- Bootstrap core JavaScript
    ================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->
	<script src="/web/resources/js/bootstrap.min.js"></script>
	<!-- Just to make our placeholder images work. Don't actually copy the next line! -->
</body>

</html>
