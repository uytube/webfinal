<jsp:include page="header.jsp"></jsp:include>
<body>
<body>
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-xs-12">
				<h4 class="text-xs-center">Iniciar sesion</h4>
			</div>
			<form action="/web/login" method="post">
				<div class="col-md-12 col-xs-12">
					<div class="form-group">
						<input type="text" class="form-control" placeholder="Usuario"
							required="required" name="user" />
					</div>
					<div class="form-group">
						<input name="password" type="password" placeholder="Password"
							required="required" class="form-control" autocomplete="off" />
					</div>
					<div class="form-group">
						<label>
							<input type="checkbox">Recordarme
						</label>
					</div>

				</div>
				<div class="col-md-12 col-xs-12">
					<button type="submit" id="submit" class="btn btn-success btn-login">Iniciar
						sesion</button>
				</div>
			</form>
		</div>
	</div>
	<div class="clearfix"></div>
	<script src="/web/resources/js/bootstrap.min.js"></script>
</body>
</html>
<style>
.btn-login {
	width: 100%;
	background: #03A9F4;
	border: 0;
	background: linear-gradient(#03A9F4, #0c92cf);
}
body{
height:100%;
background: #d3d1d1!important;
}
</style>