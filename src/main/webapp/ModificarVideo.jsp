<%@ page import="java.util.ArrayList"%>
<%@ page import="java.util.List"%>
<%@ page import="servicios.Video"%>
<%@ page import="servicios.Categoria"%>
<%@ page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<meta charset="ISO-8859-1">
<title>UyTube | Modificar video</title>
</head>
<jsp:include page="header.jsp"></jsp:include>
<body class="uploadvideo">
	<jsp:include page="sideBar.jsp"></jsp:include>
	<%
		Video V = (Video) request.getAttribute("Video");
	%>

	<div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
		<div class="container-fluid">
			<h2>Modificar video</h2>
			<form action="/web/modificarVideo" method="post">
			<input type="hidden" name="id" value="<%= V.getId() %>">
				<div class="row">
					<div class="col-md-12 col-xs-12">
						<div class="form-group">
							<label>Nombre</label> <input class="form-control" type="text"
								size="80%" name="nombre" value="<%=V.getNombre()%>">
						</div>
					</div>
					<div class="col-md-12 col-xs-12">
						<div class="form-group">
							<label>Descripcion</label>
							<textarea class="form-control" name="descripcion" rows="5"
								cols="79">  <%=V.getDescripcion()%> </textarea>
						</div>
					</div>
					<div class="col-md-12 col-xs-12">
						<div class="form-group">
							<select class="form-control" name="categoria"
								value="<%=V.getCategoria().getNombre()%>">
								<%
									for (Categoria C : (Categoria []) request.getAttribute("categorias")) {
								%>

								<option value="<%=C.getNombre()%>"><%=C.getNombre()%></option>
								<%
									}
								%>
							</select>
						</div>
					</div>
					<div class="col-md-12 col-xs-12">
						<div class="form-group">
							<label>Privacidad</label> <select name="privacidad"
								value="<%=V.isEs_publico()%>">
								<option value="true">Publico</option>
								<option value="false">Privado</option>
							</select>

						</div>
					</div>
					<div class="col-md-12 col-xs-12">
						<div class="form-group">
							<button class="btn btn-success">Editar</button>
						</div>
					</div>
				</div>
			</form>
		</div>
</body>

</html>
