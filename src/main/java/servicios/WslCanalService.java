/**
 * WslCanalService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servicios;

public interface WslCanalService extends javax.xml.rpc.Service {
    public java.lang.String getWslCanalPortAddress();

    public servicios.WslCanal getWslCanalPort() throws javax.xml.rpc.ServiceException;

    public servicios.WslCanal getWslCanalPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
