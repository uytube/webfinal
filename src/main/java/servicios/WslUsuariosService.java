/**
 * WslUsuariosService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servicios;

public interface WslUsuariosService extends javax.xml.rpc.Service {
    public java.lang.String getWslUsuariosPortAddress();

    public servicios.WslUsuarios getWslUsuariosPort() throws javax.xml.rpc.ServiceException;

    public servicios.WslUsuarios getWslUsuariosPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
