/**
 * WslCanal.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servicios;

public interface WslCanal extends java.rmi.Remote {
    public void actualizarCanal(servicios.Canal arg0) throws java.rmi.RemoteException;
    public servicios.Canal obtenerCanalUsuario(java.lang.String arg0) throws java.rmi.RemoteException;
    public servicios.Canal[] searchCanales(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
}
