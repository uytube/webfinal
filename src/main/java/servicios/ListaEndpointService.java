/**
 * ListaEndpointService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servicios;

public interface ListaEndpointService extends javax.xml.rpc.Service {
    public java.lang.String getListaEndpointPortAddress();

    public servicios.ListaEndpoint getListaEndpointPort() throws javax.xml.rpc.ServiceException;

    public servicios.ListaEndpoint getListaEndpointPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
