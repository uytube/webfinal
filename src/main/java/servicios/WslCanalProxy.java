package servicios;

public class WslCanalProxy implements servicios.WslCanal {
  private String _endpoint = null;
  private servicios.WslCanal wslCanal = null;
  
  public WslCanalProxy() {
    _initWslCanalProxy();
  }
  
  public WslCanalProxy(String endpoint) {
    _endpoint = endpoint;
    _initWslCanalProxy();
  }
  
  private void _initWslCanalProxy() {
    try {
      wslCanal = (new servicios.WslCanalServiceLocator()).getWslCanalPort();
      if (wslCanal != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)wslCanal)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)wslCanal)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (wslCanal != null)
      ((javax.xml.rpc.Stub)wslCanal)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public servicios.WslCanal getWslCanal() {
    if (wslCanal == null)
      _initWslCanalProxy();
    return wslCanal;
  }
  
  public servicios.Canal obtenerCanalUsuario(java.lang.String arg0) throws java.rmi.RemoteException{
    if (wslCanal == null)
      _initWslCanalProxy();
    return wslCanal.obtenerCanalUsuario(arg0);
  }
  
  public void actualizarCanal(servicios.Canal arg0) throws java.rmi.RemoteException{
    if (wslCanal == null)
      _initWslCanalProxy();
    wslCanal.actualizarCanal(arg0);
  }
  
  public servicios.Canal[] searchCanales(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (wslCanal == null)
      _initWslCanalProxy();
    return wslCanal.searchCanales(arg0, arg1);
  }
  
  
}