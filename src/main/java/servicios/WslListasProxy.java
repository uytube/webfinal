package servicios;

public class WslListasProxy implements servicios.WslListas {
  private String _endpoint = null;
  private servicios.WslListas wslListas = null;
  
  public WslListasProxy() {
    _initWslListasProxy();
  }
  
  public WslListasProxy(String endpoint) {
    _endpoint = endpoint;
    _initWslListasProxy();
  }
  
  private void _initWslListasProxy() {
    try {
      wslListas = (new servicios.WslListasServiceLocator()).getWslListasPort();
      if (wslListas != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)wslListas)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)wslListas)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (wslListas != null)
      ((javax.xml.rpc.Stub)wslListas)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public servicios.WslListas getWslListas() {
    if (wslListas == null)
      _initWslListasProxy();
    return wslListas;
  }
  
  public servicios.Lista[] searchListas(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (wslListas == null)
      _initWslListasProxy();
    return wslListas.searchListas(arg0, arg1);
  }
  
  public servicios.Lista[] listarListas(java.lang.String arg0) throws java.rmi.RemoteException{
    if (wslListas == null)
      _initWslListasProxy();
    return wslListas.listarListas(arg0);
  }
  
  
}