/**
 * CategoriaEndpointServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servicios;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class CategoriaEndpointServiceLocator extends org.apache.axis.client.Service
		implements servicios.CategoriaEndpointService {

	public CategoriaEndpointServiceLocator() {
	}

	public CategoriaEndpointServiceLocator(org.apache.axis.EngineConfiguration config) {
		super(config);
	}

	public CategoriaEndpointServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName)
			throws javax.xml.rpc.ServiceException {
		super(wsdlLoc, sName);
	}

	// Use to get a proxy class for CategoriaEndpointPort
	private java.lang.String CategoriaEndpointPort_address = "http://localhost:9879/categoria";
//	private java.lang.String CategoriaEndpointPort_address = "http://"+lecturaIP.lecturaIP()+":9879/categoria";

	public java.lang.String getCategoriaEndpointPortAddress() {
		return CategoriaEndpointPort_address;
	}

	// The WSDD service name defaults to the port name.
	private java.lang.String CategoriaEndpointPortWSDDServiceName = "CategoriaEndpointPort";

	public java.lang.String getCategoriaEndpointPortWSDDServiceName() {
		return CategoriaEndpointPortWSDDServiceName;
	}

	public void setCategoriaEndpointPortWSDDServiceName(java.lang.String name) {
		CategoriaEndpointPortWSDDServiceName = name;
	}

	public servicios.CategoriaEndpoint getCategoriaEndpointPort() throws javax.xml.rpc.ServiceException {
		java.net.URL endpoint;
		try {
			endpoint = new java.net.URL(CategoriaEndpointPort_address);
		} catch (java.net.MalformedURLException e) {
			throw new javax.xml.rpc.ServiceException(e);
		}
		return getCategoriaEndpointPort(endpoint);
	}

	public servicios.CategoriaEndpoint getCategoriaEndpointPort(java.net.URL portAddress)
			throws javax.xml.rpc.ServiceException {
		try {
			servicios.CategoriaEndpointPortBindingStub _stub = new servicios.CategoriaEndpointPortBindingStub(
					portAddress, this);
			_stub.setPortName(getCategoriaEndpointPortWSDDServiceName());
			return _stub;
		} catch (org.apache.axis.AxisFault e) {
			return null;
		}
	}

	public void setCategoriaEndpointPortEndpointAddress(java.lang.String address) {
		CategoriaEndpointPort_address = address;
	}

	/**
	 * For the given interface, get the stub implementation. If this service has no
	 * port for the given interface, then ServiceException is thrown.
	 */
	public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
		try {
			if (servicios.CategoriaEndpoint.class.isAssignableFrom(serviceEndpointInterface)) {
				servicios.CategoriaEndpointPortBindingStub _stub = new servicios.CategoriaEndpointPortBindingStub(
						new java.net.URL(CategoriaEndpointPort_address), this);
				_stub.setPortName(getCategoriaEndpointPortWSDDServiceName());
				return _stub;
			}
		} catch (java.lang.Throwable t) {
			throw new javax.xml.rpc.ServiceException(t);
		}
		throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  "
				+ (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
	}

	/**
	 * For the given interface, get the stub implementation. If this service has no
	 * port for the given interface, then ServiceException is thrown.
	 */
	public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface)
			throws javax.xml.rpc.ServiceException {
		if (portName == null) {
			return getPort(serviceEndpointInterface);
		}
		java.lang.String inputPortName = portName.getLocalPart();
		if ("CategoriaEndpointPort".equals(inputPortName)) {
			return getCategoriaEndpointPort();
		} else {
			java.rmi.Remote _stub = getPort(serviceEndpointInterface);
			((org.apache.axis.client.Stub) _stub).setPortName(portName);
			return _stub;
		}
	}

	public javax.xml.namespace.QName getServiceName() {
		return new javax.xml.namespace.QName("http://servicios/", "CategoriaEndpointService");
	}

	private java.util.HashSet ports = null;

	public java.util.Iterator getPorts() {
		if (ports == null) {
			ports = new java.util.HashSet();
			ports.add(new javax.xml.namespace.QName("http://servicios/", "CategoriaEndpointPort"));
		}
		return ports.iterator();
	}

	/**
	 * Set the endpoint address for the specified port name.
	 */
	public void setEndpointAddress(java.lang.String portName, java.lang.String address)
			throws javax.xml.rpc.ServiceException {

		if ("CategoriaEndpointPort".equals(portName)) {
			setCategoriaEndpointPortEndpointAddress(address);
		} else { // Unknown Port Name
			throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
		}
	}

	/**
	 * Set the endpoint address for the specified port name.
	 */
	public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address)
			throws javax.xml.rpc.ServiceException {
		setEndpointAddress(portName.getLocalPart(), address);
	}

}
