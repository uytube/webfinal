package servicios;

public class WslUsuariosProxy implements servicios.WslUsuarios {
  private String _endpoint = null;
  private servicios.WslUsuarios wslUsuarios = null;
  
  public WslUsuariosProxy() {
    _initWslUsuariosProxy();
  }
  
  public WslUsuariosProxy(String endpoint) {
    _endpoint = endpoint;
    _initWslUsuariosProxy();
  }
  
  private void _initWslUsuariosProxy() {
    try {
      wslUsuarios = (new servicios.WslUsuariosServiceLocator()).getWslUsuariosPort();
      if (wslUsuarios != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)wslUsuarios)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)wslUsuarios)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (wslUsuarios != null)
      ((javax.xml.rpc.Stub)wslUsuarios)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public servicios.WslUsuarios getWslUsuarios() {
    if (wslUsuarios == null)
      _initWslUsuariosProxy();
    return wslUsuarios;
  }
  
  public servicios.Usuario consultarUsuario(java.lang.String arg0) throws java.rmi.RemoteException{
    if (wslUsuarios == null)
      _initWslUsuariosProxy();
    return wslUsuarios.consultarUsuario(arg0);
  }
  
  public servicios.Historial[] getHistorial(java.lang.String arg0) throws java.rmi.RemoteException{
    if (wslUsuarios == null)
      _initWslUsuariosProxy();
    return wslUsuarios.getHistorial(arg0);
  }
  
  public servicios.Usuario login(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (wslUsuarios == null)
      _initWslUsuariosProxy();
    return wslUsuarios.login(arg0, arg1);
  }
  
  public void setHistorial(servicios.Video arg0, servicios.Usuario arg1) throws java.rmi.RemoteException{
    if (wslUsuarios == null)
      _initWslUsuariosProxy();
    wslUsuarios.setHistorial(arg0, arg1);
  }
  
  public servicios.Canal[] listCanalesSeguidos(java.lang.String arg0) throws java.rmi.RemoteException{
    if (wslUsuarios == null)
      _initWslUsuariosProxy();
    return wslUsuarios.listCanalesSeguidos(arg0);
  }
  
  public servicios.Usuario[] listUsuariosSeguidores(java.lang.String arg0) throws java.rmi.RemoteException{
    if (wslUsuarios == null)
      _initWslUsuariosProxy();
    return wslUsuarios.listUsuariosSeguidores(arg0);
  }
  
  public servicios.Usuario consultarPorEmail(java.lang.String arg0) throws java.rmi.RemoteException{
    if (wslUsuarios == null)
      _initWslUsuariosProxy();
    return wslUsuarios.consultarPorEmail(arg0);
  }
  
  public void crearUsuario(servicios.Usuario arg0, servicios.Canal arg1, byte[] arg2, java.lang.String arg3) throws java.rmi.RemoteException{
    if (wslUsuarios == null)
      _initWslUsuariosProxy();
    wslUsuarios.crearUsuario(arg0, arg1, arg2, arg3);
  }
  
  public void modificarUsuario(servicios.Usuario arg0, byte[] arg1, java.lang.String arg2) throws java.rmi.RemoteException{
    if (wslUsuarios == null)
      _initWslUsuariosProxy();
    wslUsuarios.modificarUsuario(arg0, arg1, arg2);
  }
  
  
}