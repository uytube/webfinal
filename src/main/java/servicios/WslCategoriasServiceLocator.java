/**
 * WslCategoriasServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servicios;

import packageWeb.lecturaIP;

public class WslCategoriasServiceLocator extends org.apache.axis.client.Service implements servicios.WslCategoriasService {

    public WslCategoriasServiceLocator() {
    }


    public WslCategoriasServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WslCategoriasServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for WslCategoriasPort
    private java.lang.String WslCategoriasPort_address = "http://localhost:9874/categorias";
    //private java.lang.String WslCategoriasPort_address = "http://"+lecturaIP.lecturaIP()+":9874/categorias";
    public java.lang.String getWslCategoriasPortAddress() {
        return WslCategoriasPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String WslCategoriasPortWSDDServiceName = "WslCategoriasPort";

    public java.lang.String getWslCategoriasPortWSDDServiceName() {
        return WslCategoriasPortWSDDServiceName;
    }

    public void setWslCategoriasPortWSDDServiceName(java.lang.String name) {
        WslCategoriasPortWSDDServiceName = name;
    }

    public servicios.WslCategorias getWslCategoriasPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(WslCategoriasPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getWslCategoriasPort(endpoint);
    }

    public servicios.WslCategorias getWslCategoriasPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            servicios.WslCategoriasPortBindingStub _stub = new servicios.WslCategoriasPortBindingStub(portAddress, this);
            _stub.setPortName(getWslCategoriasPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setWslCategoriasPortEndpointAddress(java.lang.String address) {
        WslCategoriasPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (servicios.WslCategorias.class.isAssignableFrom(serviceEndpointInterface)) {
                servicios.WslCategoriasPortBindingStub _stub = new servicios.WslCategoriasPortBindingStub(new java.net.URL(WslCategoriasPort_address), this);
                _stub.setPortName(getWslCategoriasPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("WslCategoriasPort".equals(inputPortName)) {
            return getWslCategoriasPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://servicios/", "WslCategoriasService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://servicios/", "WslCategoriasPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("WslCategoriasPort".equals(portName)) {
            setWslCategoriasPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
