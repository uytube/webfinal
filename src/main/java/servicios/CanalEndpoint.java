/**
 * CanalEndpoint.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servicios;

public interface CanalEndpoint extends java.rmi.Remote {
    public servicios.Canal[] canalesSeguidos(java.lang.String arg0) throws java.rmi.RemoteException;
    public servicios.Canal obtenerCanalUsuario(java.lang.String arg0) throws java.rmi.RemoteException;
    public void darDeBaja(java.lang.String arg0) throws java.rmi.RemoteException;
    public void actualizarCanal(servicios.Canal arg0) throws java.rmi.RemoteException;
}
