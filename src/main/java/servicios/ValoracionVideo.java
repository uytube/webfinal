/**
 * ValoracionVideo.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servicios;

public class ValoracionVideo  implements java.io.Serializable {
    private int id;

    private servicios.Usuario usuario;

    private int valoracion;

    private servicios.Video video;

    public ValoracionVideo() {
    }

    public ValoracionVideo(
           int id,
           servicios.Usuario usuario,
           int valoracion,
           servicios.Video video) {
           this.id = id;
           this.usuario = usuario;
           this.valoracion = valoracion;
           this.video = video;
    }


    /**
     * Gets the id value for this ValoracionVideo.
     * 
     * @return id
     */
    public int getId() {
        return id;
    }


    /**
     * Sets the id value for this ValoracionVideo.
     * 
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }


    /**
     * Gets the usuario value for this ValoracionVideo.
     * 
     * @return usuario
     */
    public servicios.Usuario getUsuario() {
        return usuario;
    }


    /**
     * Sets the usuario value for this ValoracionVideo.
     * 
     * @param usuario
     */
    public void setUsuario(servicios.Usuario usuario) {
        this.usuario = usuario;
    }


    /**
     * Gets the valoracion value for this ValoracionVideo.
     * 
     * @return valoracion
     */
    public int getValoracion() {
        return valoracion;
    }


    /**
     * Sets the valoracion value for this ValoracionVideo.
     * 
     * @param valoracion
     */
    public void setValoracion(int valoracion) {
        this.valoracion = valoracion;
    }


    /**
     * Gets the video value for this ValoracionVideo.
     * 
     * @return video
     */
    public servicios.Video getVideo() {
        return video;
    }


    /**
     * Sets the video value for this ValoracionVideo.
     * 
     * @param video
     */
    public void setVideo(servicios.Video video) {
        this.video = video;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof ValoracionVideo)) return false;
        ValoracionVideo other = (ValoracionVideo) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            this.id == other.getId() &&
            ((this.usuario==null && other.getUsuario()==null) || 
             (this.usuario!=null &&
              this.usuario.equals(other.getUsuario()))) &&
            this.valoracion == other.getValoracion() &&
            ((this.video==null && other.getVideo()==null) || 
             (this.video!=null &&
              this.video.equals(other.getVideo())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        _hashCode += getId();
        if (getUsuario() != null) {
            _hashCode += getUsuario().hashCode();
        }
        _hashCode += getValoracion();
        if (getVideo() != null) {
            _hashCode += getVideo().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(ValoracionVideo.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicios/", "valoracionVideo"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuario");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicios/", "usuario"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("valoracion");
        elemField.setXmlName(new javax.xml.namespace.QName("", "valoracion"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("video");
        elemField.setXmlName(new javax.xml.namespace.QName("", "video"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicios/", "video"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
