package servicios;

public class WslCategoriasProxy implements servicios.WslCategorias {
  private String _endpoint = null;
  private servicios.WslCategorias wslCategorias = null;
  
  public WslCategoriasProxy() {
    _initWslCategoriasProxy();
  }
  
  public WslCategoriasProxy(String endpoint) {
    _endpoint = endpoint;
    _initWslCategoriasProxy();
  }
  
  private void _initWslCategoriasProxy() {
    try {
      wslCategorias = (new servicios.WslCategoriasServiceLocator()).getWslCategoriasPort();
      if (wslCategorias != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)wslCategorias)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)wslCategorias)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (wslCategorias != null)
      ((javax.xml.rpc.Stub)wslCategorias)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public servicios.WslCategorias getWslCategorias() {
    if (wslCategorias == null)
      _initWslCategoriasProxy();
    return wslCategorias;
  }
  
  public servicios.Categoria obtenerCategoria(java.lang.String arg0) throws java.rmi.RemoteException{
    if (wslCategorias == null)
      _initWslCategoriasProxy();
    return wslCategorias.obtenerCategoria(arg0);
  }
  
  public servicios.Categoria[] listarCategorias() throws java.rmi.RemoteException{
    if (wslCategorias == null)
      _initWslCategoriasProxy();
    return wslCategorias.listarCategorias();
  }
  
  
}