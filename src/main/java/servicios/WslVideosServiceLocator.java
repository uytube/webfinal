/**
 * WslVideosServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servicios;

import packageWeb.lecturaIP;

public class WslVideosServiceLocator extends org.apache.axis.client.Service implements servicios.WslVideosService {

    public WslVideosServiceLocator() {
    }


    public WslVideosServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WslVideosServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for WslVideosPort
    private java.lang.String WslVideosPort_address = "http://localhost:9874/videos";
   // private java.lang.String WslVideosPort_address = "http://"+lecturaIP.lecturaIP()+":9874/videos";
    public java.lang.String getWslVideosPortAddress() {
        return WslVideosPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String WslVideosPortWSDDServiceName = "WslVideosPort";

    public java.lang.String getWslVideosPortWSDDServiceName() {
        return WslVideosPortWSDDServiceName;
    }

    public void setWslVideosPortWSDDServiceName(java.lang.String name) {
        WslVideosPortWSDDServiceName = name;
    }

    public servicios.WslVideos getWslVideosPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(WslVideosPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getWslVideosPort(endpoint);
    }

    public servicios.WslVideos getWslVideosPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            servicios.WslVideosPortBindingStub _stub = new servicios.WslVideosPortBindingStub(portAddress, this);
            _stub.setPortName(getWslVideosPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setWslVideosPortEndpointAddress(java.lang.String address) {
        WslVideosPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (servicios.WslVideos.class.isAssignableFrom(serviceEndpointInterface)) {
                servicios.WslVideosPortBindingStub _stub = new servicios.WslVideosPortBindingStub(new java.net.URL(WslVideosPort_address), this);
                _stub.setPortName(getWslVideosPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("WslVideosPort".equals(inputPortName)) {
            return getWslVideosPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://servicios/", "WslVideosService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://servicios/", "WslVideosPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("WslVideosPort".equals(portName)) {
            setWslVideosPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
