/**
 * VideoEndpoint.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servicios;

public interface VideoEndpoint extends java.rmi.Remote {
    public void modificarVideo(servicios.Video arg0) throws java.rmi.RemoteException;
    public servicios.Video datosDelVideo(int arg0) throws java.rmi.RemoteException;
    public servicios.Video[] videosUsuario(java.lang.String arg0) throws java.rmi.RemoteException;
    public servicios.Video[] videoPorCategoria(servicios.Categoria arg0) throws java.rmi.RemoteException;
    public servicios.Video[] obtenerVideosUsuario(java.lang.String arg0) throws java.rmi.RemoteException;
    public void subirVideoYT(servicios.Video arg0, java.lang.String arg1, java.lang.String arg2) throws java.rmi.RemoteException;
}
