/**
 * WslListasServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servicios;

import packageWeb.lecturaIP;

public class WslListasServiceLocator extends org.apache.axis.client.Service implements servicios.WslListasService {

    public WslListasServiceLocator() {
    }


    public WslListasServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public WslListasServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

    // Use to get a proxy class for WslListasPort
    private java.lang.String WslListasPort_address = "http://localhost:9874/listas";
   // private java.lang.String WslListasPort_address = "http://"+lecturaIP.lecturaIP()+":9874/listas";
    public java.lang.String getWslListasPortAddress() {
        return WslListasPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String WslListasPortWSDDServiceName = "WslListasPort";

    public java.lang.String getWslListasPortWSDDServiceName() {
        return WslListasPortWSDDServiceName;
    }

    public void setWslListasPortWSDDServiceName(java.lang.String name) {
        WslListasPortWSDDServiceName = name;
    }

    public servicios.WslListas getWslListasPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(WslListasPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getWslListasPort(endpoint);
    }

    public servicios.WslListas getWslListasPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            servicios.WslListasPortBindingStub _stub = new servicios.WslListasPortBindingStub(portAddress, this);
            _stub.setPortName(getWslListasPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setWslListasPortEndpointAddress(java.lang.String address) {
        WslListasPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (servicios.WslListas.class.isAssignableFrom(serviceEndpointInterface)) {
                servicios.WslListasPortBindingStub _stub = new servicios.WslListasPortBindingStub(new java.net.URL(WslListasPort_address), this);
                _stub.setPortName(getWslListasPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("WslListasPort".equals(inputPortName)) {
            return getWslListasPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://servicios/", "WslListasService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://servicios/", "WslListasPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("WslListasPort".equals(portName)) {
            setWslListasPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
