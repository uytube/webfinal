/**
 * WslVideos.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servicios;

public interface WslVideos extends java.rmi.Remote {
    public servicios.Video[] listaVideosUsuario(java.lang.String arg0) throws java.rmi.RemoteException;
    public servicios.Video[] searchVideo(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2) throws java.rmi.RemoteException;
    public servicios.Video[] videoPorCategoria(servicios.Categoria arg0) throws java.rmi.RemoteException;
    public servicios.Video[] videosRandom() throws java.rmi.RemoteException;
}
