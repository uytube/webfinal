package servicios;

public class CanalEndpointProxy implements servicios.CanalEndpoint {
  private String _endpoint = null;
  private servicios.CanalEndpoint canalEndpoint = null;
  
  public CanalEndpointProxy() {
    _initCanalEndpointProxy();
  }
  
  public CanalEndpointProxy(String endpoint) {
    _endpoint = endpoint;
    _initCanalEndpointProxy();
  }
  
  private void _initCanalEndpointProxy() {
    try {
      canalEndpoint = (new servicios.CanalEndpointServiceLocator()).getCanalEndpointPort();
      if (canalEndpoint != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)canalEndpoint)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)canalEndpoint)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (canalEndpoint != null)
      ((javax.xml.rpc.Stub)canalEndpoint)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public servicios.CanalEndpoint getCanalEndpoint() {
    if (canalEndpoint == null)
      _initCanalEndpointProxy();
    return canalEndpoint;
  }
  
  public servicios.Canal[] canalesSeguidos(java.lang.String arg0) throws java.rmi.RemoteException{
    if (canalEndpoint == null)
      _initCanalEndpointProxy();
    return canalEndpoint.canalesSeguidos(arg0);
  }
  
  public servicios.Canal obtenerCanalUsuario(java.lang.String arg0) throws java.rmi.RemoteException{
    if (canalEndpoint == null)
      _initCanalEndpointProxy();
    return canalEndpoint.obtenerCanalUsuario(arg0);
  }
  
  public void darDeBaja(java.lang.String arg0) throws java.rmi.RemoteException{
    if (canalEndpoint == null)
      _initCanalEndpointProxy();
    canalEndpoint.darDeBaja(arg0);
  }
  
  public void actualizarCanal(servicios.Canal arg0) throws java.rmi.RemoteException{
    if (canalEndpoint == null)
      _initCanalEndpointProxy();
    canalEndpoint.actualizarCanal(arg0);
  }
  
  
}