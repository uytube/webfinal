/**
 * CategoriaEndpointService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servicios;

public interface CategoriaEndpointService extends javax.xml.rpc.Service {
    public java.lang.String getCategoriaEndpointPortAddress();

    public servicios.CategoriaEndpoint getCategoriaEndpointPort() throws javax.xml.rpc.ServiceException;

    public servicios.CategoriaEndpoint getCategoriaEndpointPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
