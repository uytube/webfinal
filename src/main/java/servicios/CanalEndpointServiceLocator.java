/**
 * CanalEndpointServiceLocator.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servicios;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

import packageWeb.lecturaIP;

public class CanalEndpointServiceLocator extends org.apache.axis.client.Service implements servicios.CanalEndpointService {

    public CanalEndpointServiceLocator() {
    }


    public CanalEndpointServiceLocator(org.apache.axis.EngineConfiguration config) {
        super(config);
    }

    public CanalEndpointServiceLocator(java.lang.String wsdlLoc, javax.xml.namespace.QName sName) throws javax.xml.rpc.ServiceException {
        super(wsdlLoc, sName);
    }

	// Use to get a proxy class for CategoriaEndpointPort
	private java.lang.String CanalEndpointPort_address = "http://localhost:9880/canal";
	//private java.lang.String CanalEndpointPort_address = "http://"+lecturaIP.lecturaIP()+":9880/canaal";

    public java.lang.String getCanalEndpointPortAddress() {
        return CanalEndpointPort_address;
    }

    // The WSDD service name defaults to the port name.
    private java.lang.String CanalEndpointPortWSDDServiceName = "CanalEndpointPort";

    public java.lang.String getCanalEndpointPortWSDDServiceName() {
        return CanalEndpointPortWSDDServiceName;
    }

    public void setCanalEndpointPortWSDDServiceName(java.lang.String name) {
        CanalEndpointPortWSDDServiceName = name;
    }

    public servicios.CanalEndpoint getCanalEndpointPort() throws javax.xml.rpc.ServiceException {
       java.net.URL endpoint;
        try {
            endpoint = new java.net.URL(CanalEndpointPort_address);
        }
        catch (java.net.MalformedURLException e) {
            throw new javax.xml.rpc.ServiceException(e);
        }
        return getCanalEndpointPort(endpoint);
    }

    public servicios.CanalEndpoint getCanalEndpointPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException {
        try {
            servicios.CanalEndpointPortBindingStub _stub = new servicios.CanalEndpointPortBindingStub(portAddress, this);
            _stub.setPortName(getCanalEndpointPortWSDDServiceName());
            return _stub;
        }
        catch (org.apache.axis.AxisFault e) {
            return null;
        }
    }

    public void setCanalEndpointPortEndpointAddress(java.lang.String address) {
        CanalEndpointPort_address = address;
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        try {
            if (servicios.CanalEndpoint.class.isAssignableFrom(serviceEndpointInterface)) {
                servicios.CanalEndpointPortBindingStub _stub = new servicios.CanalEndpointPortBindingStub(new java.net.URL(CanalEndpointPort_address), this);
                _stub.setPortName(getCanalEndpointPortWSDDServiceName());
                return _stub;
            }
        }
        catch (java.lang.Throwable t) {
            throw new javax.xml.rpc.ServiceException(t);
        }
        throw new javax.xml.rpc.ServiceException("There is no stub implementation for the interface:  " + (serviceEndpointInterface == null ? "null" : serviceEndpointInterface.getName()));
    }

    /**
     * For the given interface, get the stub implementation.
     * If this service has no port for the given interface,
     * then ServiceException is thrown.
     */
    public java.rmi.Remote getPort(javax.xml.namespace.QName portName, Class serviceEndpointInterface) throws javax.xml.rpc.ServiceException {
        if (portName == null) {
            return getPort(serviceEndpointInterface);
        }
        java.lang.String inputPortName = portName.getLocalPart();
        if ("CanalEndpointPort".equals(inputPortName)) {
            return getCanalEndpointPort();
        }
        else  {
            java.rmi.Remote _stub = getPort(serviceEndpointInterface);
            ((org.apache.axis.client.Stub) _stub).setPortName(portName);
            return _stub;
        }
    }

    public javax.xml.namespace.QName getServiceName() {
        return new javax.xml.namespace.QName("http://servicios/", "CanalEndpointService");
    }

    private java.util.HashSet ports = null;

    public java.util.Iterator getPorts() {
        if (ports == null) {
            ports = new java.util.HashSet();
            ports.add(new javax.xml.namespace.QName("http://servicios/", "CanalEndpointPort"));
        }
        return ports.iterator();
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(java.lang.String portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        
if ("CanalEndpointPort".equals(portName)) {
            setCanalEndpointPortEndpointAddress(address);
        }
        else 
{ // Unknown Port Name
            throw new javax.xml.rpc.ServiceException(" Cannot set Endpoint Address for Unknown Port" + portName);
        }
    }

    /**
    * Set the endpoint address for the specified port name.
    */
    public void setEndpointAddress(javax.xml.namespace.QName portName, java.lang.String address) throws javax.xml.rpc.ServiceException {
        setEndpointAddress(portName.getLocalPart(), address);
    }

}
