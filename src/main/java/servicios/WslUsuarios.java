/**
 * WslUsuarios.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servicios;

public interface WslUsuarios extends java.rmi.Remote {
    public servicios.Canal[] listCanalesSeguidos(java.lang.String arg0) throws java.rmi.RemoteException;
    public servicios.Usuario[] listUsuariosSeguidores(java.lang.String arg0) throws java.rmi.RemoteException;
    public servicios.Usuario consultarUsuario(java.lang.String arg0) throws java.rmi.RemoteException;
    public servicios.Usuario login(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public servicios.Historial[] getHistorial(java.lang.String arg0) throws java.rmi.RemoteException;
    public void modificarUsuario(servicios.Usuario arg0, byte[] arg1, java.lang.String arg2) throws java.rmi.RemoteException;
    public void crearUsuario(servicios.Usuario arg0, servicios.Canal arg1, byte[] arg2, java.lang.String arg3) throws java.rmi.RemoteException;
    public void setHistorial(servicios.Video arg0, servicios.Usuario arg1) throws java.rmi.RemoteException;
    public servicios.Usuario consultarPorEmail(java.lang.String arg0) throws java.rmi.RemoteException;
}
