/**
 * Ver.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servicios;

public interface Ver extends java.rmi.Remote {
    public servicios.Lista[] listarListas(java.lang.String arg0) throws java.rmi.RemoteException;
    public servicios.Categoria[] listaDeCategorias() throws java.rmi.RemoteException;
    public servicios.Video datosDelVideo(int arg0) throws java.rmi.RemoteException;
    public servicios.Canal[] canalesSeguidos(java.lang.String arg0) throws java.rmi.RemoteException;
    public servicios.ValoracionVideo[] valoracionesVideo(int arg0) throws java.rmi.RemoteException;
    public servicios.ValoracionVideo devolverValoracionVideo(int arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public servicios.Video[] videosUsuario(java.lang.String arg0) throws java.rmi.RemoteException;
    public servicios.ValoracionVideo[] valoracionesDeVideo(int arg0) throws java.rmi.RemoteException;
    public servicios.Video[] obtenerVideosUsuario(java.lang.String arg0) throws java.rmi.RemoteException;
    public servicios.Comentario[] comentariosDeUnVideo(java.lang.String arg0) throws java.rmi.RemoteException;
    public void postSeguirUsuario(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public void postDejarSeguirUsuario(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException;
    public void postAgregarVideo(int arg0, int arg1) throws java.rmi.RemoteException;
    public java.lang.String agregarVideo(int arg0, int arg1) throws java.rmi.RemoteException;
    public void postQuitarVideo(int arg0, int arg1) throws java.rmi.RemoteException;
    public java.lang.String quitarVideo(int arg0, int arg1) throws java.rmi.RemoteException;
    public java.lang.String postValorarVideo(int arg0, java.lang.String arg1, int arg2) throws java.rmi.RemoteException;
    public java.lang.String postComentarioVideo(int arg0, java.lang.String arg1, int arg2, java.lang.String arg3) throws java.rmi.RemoteException;
    public void postCrearLista(java.lang.String arg0, java.lang.String arg1, boolean arg2, java.lang.String arg3) throws java.rmi.RemoteException;
}
