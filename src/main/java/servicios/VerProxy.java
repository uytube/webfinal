package servicios;

public class VerProxy implements servicios.Ver {
  private String _endpoint = null;
  private servicios.Ver ver = null;
  
  public VerProxy() {
    _initVerProxy();
  }
  
  public VerProxy(String endpoint) {
    _endpoint = endpoint;
    _initVerProxy();
  }
  
  private void _initVerProxy() {
    try {
      ver = (new servicios.VerServiceLocator()).getverPort();
      if (ver != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)ver)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)ver)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (ver != null)
      ((javax.xml.rpc.Stub)ver)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public servicios.Ver getVer() {
    if (ver == null)
      _initVerProxy();
    return ver;
  }
  
  public servicios.Categoria[] listaDeCategorias() throws java.rmi.RemoteException{
    if (ver == null)
      _initVerProxy();
    return ver.listaDeCategorias();
  }
  
  public servicios.Video datosDelVideo(int arg0) throws java.rmi.RemoteException{
    if (ver == null)
      _initVerProxy();
    return ver.datosDelVideo(arg0);
  }
  
  public servicios.Canal[] canalesSeguidos(java.lang.String arg0) throws java.rmi.RemoteException{
    if (ver == null)
      _initVerProxy();
    return ver.canalesSeguidos(arg0);
  }
  
  public servicios.ValoracionVideo[] valoracionesVideo(int arg0) throws java.rmi.RemoteException{
    if (ver == null)
      _initVerProxy();
    return ver.valoracionesVideo(arg0);
  }
  
  public servicios.ValoracionVideo devolverValoracionVideo(int arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (ver == null)
      _initVerProxy();
    return ver.devolverValoracionVideo(arg0, arg1);
  }
  
  public servicios.Video[] videosUsuario(java.lang.String arg0) throws java.rmi.RemoteException{
    if (ver == null)
      _initVerProxy();
    return ver.videosUsuario(arg0);
  }
  
  public servicios.ValoracionVideo[] valoracionesDeVideo(int arg0) throws java.rmi.RemoteException{
    if (ver == null)
      _initVerProxy();
    return ver.valoracionesDeVideo(arg0);
  }
  
  public servicios.Video[] obtenerVideosUsuario(java.lang.String arg0) throws java.rmi.RemoteException{
    if (ver == null)
      _initVerProxy();
    return ver.obtenerVideosUsuario(arg0);
  }
  
  public servicios.Comentario[] comentariosDeUnVideo(java.lang.String arg0) throws java.rmi.RemoteException{
    if (ver == null)
      _initVerProxy();
    return ver.comentariosDeUnVideo(arg0);
  }
  
  public void postSeguirUsuario(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (ver == null)
      _initVerProxy();
    ver.postSeguirUsuario(arg0, arg1);
  }
  
  public void postDejarSeguirUsuario(java.lang.String arg0, java.lang.String arg1) throws java.rmi.RemoteException{
    if (ver == null)
      _initVerProxy();
    ver.postDejarSeguirUsuario(arg0, arg1);
  }
  
  public void postAgregarVideo(int arg0, int arg1) throws java.rmi.RemoteException{
    if (ver == null)
      _initVerProxy();
    ver.postAgregarVideo(arg0, arg1);
  }
  
  public java.lang.String agregarVideo(int arg0, int arg1) throws java.rmi.RemoteException{
    if (ver == null)
      _initVerProxy();
    return ver.agregarVideo(arg0, arg1);
  }
  
  public void postQuitarVideo(int arg0, int arg1) throws java.rmi.RemoteException{
    if (ver == null)
      _initVerProxy();
    ver.postQuitarVideo(arg0, arg1);
  }
  
  public java.lang.String quitarVideo(int arg0, int arg1) throws java.rmi.RemoteException{
    if (ver == null)
      _initVerProxy();
    return ver.quitarVideo(arg0, arg1);
  }
  
  public java.lang.String postValorarVideo(int arg0, java.lang.String arg1, int arg2) throws java.rmi.RemoteException{
    if (ver == null)
      _initVerProxy();
    return ver.postValorarVideo(arg0, arg1, arg2);
  }
  
  public java.lang.String postComentarioVideo(int arg0, java.lang.String arg1, int arg2, java.lang.String arg3) throws java.rmi.RemoteException{
    if (ver == null)
      _initVerProxy();
    return ver.postComentarioVideo(arg0, arg1, arg2, arg3);
  }
  
  public void postCrearLista(java.lang.String arg0, java.lang.String arg1, boolean arg2, java.lang.String arg3) throws java.rmi.RemoteException{
    if (ver == null)
      _initVerProxy();
    ver.postCrearLista(arg0, arg1, arg2, arg3);
  }
  
  public servicios.Lista[] listarListas(java.lang.String arg0) throws java.rmi.RemoteException{
    if (ver == null)
      _initVerProxy();
    return ver.listarListas(arg0);
  }
  
  
}