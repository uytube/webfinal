package servicios;

public class WslVideosProxy implements servicios.WslVideos {
  private String _endpoint = null;
  private servicios.WslVideos wslVideos = null;
  
  public WslVideosProxy() {
    _initWslVideosProxy();
  }
  
  public WslVideosProxy(String endpoint) {
    _endpoint = endpoint;
    _initWslVideosProxy();
  }
  
  private void _initWslVideosProxy() {
    try {
      wslVideos = (new servicios.WslVideosServiceLocator()).getWslVideosPort();
      if (wslVideos != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)wslVideos)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)wslVideos)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (wslVideos != null)
      ((javax.xml.rpc.Stub)wslVideos)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public servicios.WslVideos getWslVideos() {
    if (wslVideos == null)
      _initWslVideosProxy();
    return wslVideos;
  }
  
  public servicios.Video[] listaVideosUsuario(java.lang.String arg0) throws java.rmi.RemoteException{
    if (wslVideos == null)
      _initWslVideosProxy();
    return wslVideos.listaVideosUsuario(arg0);
  }
  
  public servicios.Video[] searchVideo(java.lang.String arg0, java.lang.String arg1, java.lang.String arg2) throws java.rmi.RemoteException{
    if (wslVideos == null)
      _initWslVideosProxy();
    return wslVideos.searchVideo(arg0, arg1, arg2);
  }
  
  public servicios.Video[] videoPorCategoria(servicios.Categoria arg0) throws java.rmi.RemoteException{
    if (wslVideos == null)
      _initWslVideosProxy();
    return wslVideos.videoPorCategoria(arg0);
  }
  
  public servicios.Video[] videosRandom() throws java.rmi.RemoteException{
    if (wslVideos == null)
      _initWslVideosProxy();
    return wslVideos.videosRandom();
  }
  
  
}