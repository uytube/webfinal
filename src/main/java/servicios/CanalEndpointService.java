/**
 * CanalEndpointService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servicios;

public interface CanalEndpointService extends javax.xml.rpc.Service {
    public java.lang.String getCanalEndpointPortAddress();

    public servicios.CanalEndpoint getCanalEndpointPort() throws javax.xml.rpc.ServiceException;

    public servicios.CanalEndpoint getCanalEndpointPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
