/**
 * ListaEndpoint.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servicios;

public interface ListaEndpoint extends java.rmi.Remote {
    public servicios.Video[] videosListaReproduccion(int arg0) throws java.rmi.RemoteException;
    public void postCrearLista(java.lang.String arg0, java.lang.String arg1, boolean arg2, java.lang.String arg3) throws java.rmi.RemoteException;
    public java.lang.String agregarVideo(int arg0, int arg1) throws java.rmi.RemoteException;
    public servicios.Lista[] listarListas(java.lang.String arg0) throws java.rmi.RemoteException;
    public java.lang.String quitarVideo(int arg0, int arg1) throws java.rmi.RemoteException;
    public void modificarLista(int arg0, boolean arg1) throws java.rmi.RemoteException;
    public servicios.Lista listaPorID(int arg0) throws java.rmi.RemoteException;
}
