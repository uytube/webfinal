/**
 * VideoEndpointService.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servicios;

public interface VideoEndpointService extends javax.xml.rpc.Service {
    public java.lang.String getVideoEndpointPortAddress();

    public servicios.VideoEndpoint getVideoEndpointPort() throws javax.xml.rpc.ServiceException;

    public servicios.VideoEndpoint getVideoEndpointPort(java.net.URL portAddress) throws javax.xml.rpc.ServiceException;
}
