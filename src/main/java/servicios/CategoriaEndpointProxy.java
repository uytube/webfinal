package servicios;

public class CategoriaEndpointProxy implements servicios.CategoriaEndpoint {
  private String _endpoint = null;
  private servicios.CategoriaEndpoint categoriaEndpoint = null;
  
  public CategoriaEndpointProxy() {
    _initCategoriaEndpointProxy();
  }
  
  public CategoriaEndpointProxy(String endpoint) {
    _endpoint = endpoint;
    _initCategoriaEndpointProxy();
  }
  
  private void _initCategoriaEndpointProxy() {
    try {
      categoriaEndpoint = (new servicios.CategoriaEndpointServiceLocator()).getCategoriaEndpointPort();
      if (categoriaEndpoint != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)categoriaEndpoint)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)categoriaEndpoint)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (categoriaEndpoint != null)
      ((javax.xml.rpc.Stub)categoriaEndpoint)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public servicios.CategoriaEndpoint getCategoriaEndpoint() {
    if (categoriaEndpoint == null)
      _initCategoriaEndpointProxy();
    return categoriaEndpoint;
  }
  
  public servicios.Categoria obtenerCategoria(java.lang.String arg0) throws java.rmi.RemoteException{
    if (categoriaEndpoint == null)
      _initCategoriaEndpointProxy();
    return categoriaEndpoint.obtenerCategoria(arg0);
  }
  
  public servicios.Categoria[] listaDeCategorias() throws java.rmi.RemoteException{
    if (categoriaEndpoint == null)
      _initCategoriaEndpointProxy();
    return categoriaEndpoint.listaDeCategorias();
  }
  
  public servicios.Categoria obtenerCategoriaPorID(int arg0) throws java.rmi.RemoteException{
    if (categoriaEndpoint == null)
      _initCategoriaEndpointProxy();
    return categoriaEndpoint.obtenerCategoriaPorID(arg0);
  }
  
  
}