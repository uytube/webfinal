/**
 * Lista.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servicios;

public class Lista  implements java.io.Serializable {
    private servicios.Canal canal;

    private servicios.Categoria categoria;

    private boolean deleted;

    private int id;

    private java.lang.String nombre;

    private java.lang.String nombre_lista;

    private java.lang.Boolean privado;

    private java.lang.Boolean tipo;

    private servicios.Video[] videos;

    public Lista() {
    }

    public Lista(
           servicios.Canal canal,
           servicios.Categoria categoria,
           boolean deleted,
           int id,
           java.lang.String nombre,
           java.lang.String nombre_lista,
           java.lang.Boolean privado,
           java.lang.Boolean tipo,
           servicios.Video[] videos) {
           this.canal = canal;
           this.categoria = categoria;
           this.deleted = deleted;
           this.id = id;
           this.nombre = nombre;
           this.nombre_lista = nombre_lista;
           this.privado = privado;
           this.tipo = tipo;
           this.videos = videos;
    }


    /**
     * Gets the canal value for this Lista.
     * 
     * @return canal
     */
    public servicios.Canal getCanal() {
        return canal;
    }


    /**
     * Sets the canal value for this Lista.
     * 
     * @param canal
     */
    public void setCanal(servicios.Canal canal) {
        this.canal = canal;
    }


    /**
     * Gets the categoria value for this Lista.
     * 
     * @return categoria
     */
    public servicios.Categoria getCategoria() {
        return categoria;
    }


    /**
     * Sets the categoria value for this Lista.
     * 
     * @param categoria
     */
    public void setCategoria(servicios.Categoria categoria) {
        this.categoria = categoria;
    }


    /**
     * Gets the deleted value for this Lista.
     * 
     * @return deleted
     */
    public boolean isDeleted() {
        return deleted;
    }


    /**
     * Sets the deleted value for this Lista.
     * 
     * @param deleted
     */
    public void setDeleted(boolean deleted) {
        this.deleted = deleted;
    }


    /**
     * Gets the id value for this Lista.
     * 
     * @return id
     */
    public int getId() {
        return id;
    }


    /**
     * Sets the id value for this Lista.
     * 
     * @param id
     */
    public void setId(int id) {
        this.id = id;
    }


    /**
     * Gets the nombre value for this Lista.
     * 
     * @return nombre
     */
    public java.lang.String getNombre() {
        return nombre;
    }


    /**
     * Sets the nombre value for this Lista.
     * 
     * @param nombre
     */
    public void setNombre(java.lang.String nombre) {
        this.nombre = nombre;
    }


    /**
     * Gets the nombre_lista value for this Lista.
     * 
     * @return nombre_lista
     */
    public java.lang.String getNombre_lista() {
        return nombre_lista;
    }


    /**
     * Sets the nombre_lista value for this Lista.
     * 
     * @param nombre_lista
     */
    public void setNombre_lista(java.lang.String nombre_lista) {
        this.nombre_lista = nombre_lista;
    }


    /**
     * Gets the privado value for this Lista.
     * 
     * @return privado
     */
    public java.lang.Boolean getPrivado() {
        return privado;
    }


    /**
     * Sets the privado value for this Lista.
     * 
     * @param privado
     */
    public void setPrivado(java.lang.Boolean privado) {
        this.privado = privado;
    }


    /**
     * Gets the tipo value for this Lista.
     * 
     * @return tipo
     */
    public java.lang.Boolean getTipo() {
        return tipo;
    }


    /**
     * Sets the tipo value for this Lista.
     * 
     * @param tipo
     */
    public void setTipo(java.lang.Boolean tipo) {
        this.tipo = tipo;
    }


    /**
     * Gets the videos value for this Lista.
     * 
     * @return videos
     */
    public servicios.Video[] getVideos() {
        return videos;
    }


    /**
     * Sets the videos value for this Lista.
     * 
     * @param videos
     */
    public void setVideos(servicios.Video[] videos) {
        this.videos = videos;
    }

    public servicios.Video getVideos(int i) {
        return this.videos[i];
    }

    public void setVideos(int i, servicios.Video _value) {
        this.videos[i] = _value;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Lista)) return false;
        Lista other = (Lista) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.canal==null && other.getCanal()==null) || 
             (this.canal!=null &&
              this.canal.equals(other.getCanal()))) &&
            ((this.categoria==null && other.getCategoria()==null) || 
             (this.categoria!=null &&
              this.categoria.equals(other.getCategoria()))) &&
            this.deleted == other.isDeleted() &&
            this.id == other.getId() &&
            ((this.nombre==null && other.getNombre()==null) || 
             (this.nombre!=null &&
              this.nombre.equals(other.getNombre()))) &&
            ((this.nombre_lista==null && other.getNombre_lista()==null) || 
             (this.nombre_lista!=null &&
              this.nombre_lista.equals(other.getNombre_lista()))) &&
            ((this.privado==null && other.getPrivado()==null) || 
             (this.privado!=null &&
              this.privado.equals(other.getPrivado()))) &&
            ((this.tipo==null && other.getTipo()==null) || 
             (this.tipo!=null &&
              this.tipo.equals(other.getTipo()))) &&
            ((this.videos==null && other.getVideos()==null) || 
             (this.videos!=null &&
              java.util.Arrays.equals(this.videos, other.getVideos())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getCanal() != null) {
            _hashCode += getCanal().hashCode();
        }
        if (getCategoria() != null) {
            _hashCode += getCategoria().hashCode();
        }
        _hashCode += (isDeleted() ? Boolean.TRUE : Boolean.FALSE).hashCode();
        _hashCode += getId();
        if (getNombre() != null) {
            _hashCode += getNombre().hashCode();
        }
        if (getNombre_lista() != null) {
            _hashCode += getNombre_lista().hashCode();
        }
        if (getPrivado() != null) {
            _hashCode += getPrivado().hashCode();
        }
        if (getTipo() != null) {
            _hashCode += getTipo().hashCode();
        }
        if (getVideos() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getVideos());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getVideos(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Lista.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicios/", "lista"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("canal");
        elemField.setXmlName(new javax.xml.namespace.QName("", "canal"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicios/", "canal"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("categoria");
        elemField.setXmlName(new javax.xml.namespace.QName("", "categoria"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicios/", "categoria"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("deleted");
        elemField.setXmlName(new javax.xml.namespace.QName("", "deleted"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "int"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nombre");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nombre"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("nombre_lista");
        elemField.setXmlName(new javax.xml.namespace.QName("", "nombre_lista"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("privado");
        elemField.setXmlName(new javax.xml.namespace.QName("", "privado"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("tipo");
        elemField.setXmlName(new javax.xml.namespace.QName("", "tipo"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "boolean"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("videos");
        elemField.setXmlName(new javax.xml.namespace.QName("", "videos"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicios/", "video"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
