/**
 * Comentario.java
 *
 * This file was auto-generated from WSDL
 * by the Apache Axis 1.4 Apr 22, 2006 (06:55:48 PDT) WSDL2Java emitter.
 */

package servicios;

public class Comentario  implements java.io.Serializable {
    private java.lang.String comentario;

    private java.util.Calendar fecha;

    private long id;

    private servicios.Comentario[] respuestas;

    private servicios.Usuario usuario;

    private servicios.Video vid;

    public Comentario() {
    }

    public Comentario(
           java.lang.String comentario,
           java.util.Calendar fecha,
           long id,
           servicios.Comentario[] respuestas,
           servicios.Usuario usuario,
           servicios.Video vid) {
           this.comentario = comentario;
           this.fecha = fecha;
           this.id = id;
           this.respuestas = respuestas;
           this.usuario = usuario;
           this.vid = vid;
    }


    /**
     * Gets the comentario value for this Comentario.
     * 
     * @return comentario
     */
    public java.lang.String getComentario() {
        return comentario;
    }


    /**
     * Sets the comentario value for this Comentario.
     * 
     * @param comentario
     */
    public void setComentario(java.lang.String comentario) {
        this.comentario = comentario;
    }


    /**
     * Gets the fecha value for this Comentario.
     * 
     * @return fecha
     */
    public java.util.Calendar getFecha() {
        return fecha;
    }


    /**
     * Sets the fecha value for this Comentario.
     * 
     * @param fecha
     */
    public void setFecha(java.util.Calendar fecha) {
        this.fecha = fecha;
    }


    /**
     * Gets the id value for this Comentario.
     * 
     * @return id
     */
    public long getId() {
        return id;
    }


    /**
     * Sets the id value for this Comentario.
     * 
     * @param id
     */
    public void setId(long id) {
        this.id = id;
    }


    /**
     * Gets the respuestas value for this Comentario.
     * 
     * @return respuestas
     */
    public servicios.Comentario[] getRespuestas() {
        return respuestas;
    }


    /**
     * Sets the respuestas value for this Comentario.
     * 
     * @param respuestas
     */
    public void setRespuestas(servicios.Comentario[] respuestas) {
        this.respuestas = respuestas;
    }

    public servicios.Comentario getRespuestas(int i) {
        return this.respuestas[i];
    }

    public void setRespuestas(int i, servicios.Comentario _value) {
        this.respuestas[i] = _value;
    }


    /**
     * Gets the usuario value for this Comentario.
     * 
     * @return usuario
     */
    public servicios.Usuario getUsuario() {
        return usuario;
    }


    /**
     * Sets the usuario value for this Comentario.
     * 
     * @param usuario
     */
    public void setUsuario(servicios.Usuario usuario) {
        this.usuario = usuario;
    }


    /**
     * Gets the vid value for this Comentario.
     * 
     * @return vid
     */
    public servicios.Video getVid() {
        return vid;
    }


    /**
     * Sets the vid value for this Comentario.
     * 
     * @param vid
     */
    public void setVid(servicios.Video vid) {
        this.vid = vid;
    }

    private java.lang.Object __equalsCalc = null;
    public synchronized boolean equals(java.lang.Object obj) {
        if (!(obj instanceof Comentario)) return false;
        Comentario other = (Comentario) obj;
        if (obj == null) return false;
        if (this == obj) return true;
        if (__equalsCalc != null) {
            return (__equalsCalc == obj);
        }
        __equalsCalc = obj;
        boolean _equals;
        _equals = true && 
            ((this.comentario==null && other.getComentario()==null) || 
             (this.comentario!=null &&
              this.comentario.equals(other.getComentario()))) &&
            ((this.fecha==null && other.getFecha()==null) || 
             (this.fecha!=null &&
              this.fecha.equals(other.getFecha()))) &&
            this.id == other.getId() &&
            ((this.respuestas==null && other.getRespuestas()==null) || 
             (this.respuestas!=null &&
              java.util.Arrays.equals(this.respuestas, other.getRespuestas()))) &&
            ((this.usuario==null && other.getUsuario()==null) || 
             (this.usuario!=null &&
              this.usuario.equals(other.getUsuario()))) &&
            ((this.vid==null && other.getVid()==null) || 
             (this.vid!=null &&
              this.vid.equals(other.getVid())));
        __equalsCalc = null;
        return _equals;
    }

    private boolean __hashCodeCalc = false;
    public synchronized int hashCode() {
        if (__hashCodeCalc) {
            return 0;
        }
        __hashCodeCalc = true;
        int _hashCode = 1;
        if (getComentario() != null) {
            _hashCode += getComentario().hashCode();
        }
        if (getFecha() != null) {
            _hashCode += getFecha().hashCode();
        }
        _hashCode += new Long(getId()).hashCode();
        if (getRespuestas() != null) {
            for (int i=0;
                 i<java.lang.reflect.Array.getLength(getRespuestas());
                 i++) {
                java.lang.Object obj = java.lang.reflect.Array.get(getRespuestas(), i);
                if (obj != null &&
                    !obj.getClass().isArray()) {
                    _hashCode += obj.hashCode();
                }
            }
        }
        if (getUsuario() != null) {
            _hashCode += getUsuario().hashCode();
        }
        if (getVid() != null) {
            _hashCode += getVid().hashCode();
        }
        __hashCodeCalc = false;
        return _hashCode;
    }

    // Type metadata
    private static org.apache.axis.description.TypeDesc typeDesc =
        new org.apache.axis.description.TypeDesc(Comentario.class, true);

    static {
        typeDesc.setXmlType(new javax.xml.namespace.QName("http://servicios/", "comentario"));
        org.apache.axis.description.ElementDesc elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("comentario");
        elemField.setXmlName(new javax.xml.namespace.QName("", "comentario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "string"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("fecha");
        elemField.setXmlName(new javax.xml.namespace.QName("", "fecha"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "dateTime"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("id");
        elemField.setXmlName(new javax.xml.namespace.QName("", "id"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://www.w3.org/2001/XMLSchema", "long"));
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("respuestas");
        elemField.setXmlName(new javax.xml.namespace.QName("", "respuestas"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicios/", "comentario"));
        elemField.setMinOccurs(0);
        elemField.setNillable(true);
        elemField.setMaxOccursUnbounded(true);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("usuario");
        elemField.setXmlName(new javax.xml.namespace.QName("", "usuario"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicios/", "usuario"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
        elemField = new org.apache.axis.description.ElementDesc();
        elemField.setFieldName("vid");
        elemField.setXmlName(new javax.xml.namespace.QName("", "vid"));
        elemField.setXmlType(new javax.xml.namespace.QName("http://servicios/", "video"));
        elemField.setMinOccurs(0);
        elemField.setNillable(false);
        typeDesc.addFieldDesc(elemField);
    }

    /**
     * Return type metadata object
     */
    public static org.apache.axis.description.TypeDesc getTypeDesc() {
        return typeDesc;
    }

    /**
     * Get Custom Serializer
     */
    public static org.apache.axis.encoding.Serializer getSerializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanSerializer(
            _javaType, _xmlType, typeDesc);
    }

    /**
     * Get Custom Deserializer
     */
    public static org.apache.axis.encoding.Deserializer getDeserializer(
           java.lang.String mechType, 
           java.lang.Class _javaType,  
           javax.xml.namespace.QName _xmlType) {
        return 
          new  org.apache.axis.encoding.ser.BeanDeserializer(
            _javaType, _xmlType, typeDesc);
    }

}
