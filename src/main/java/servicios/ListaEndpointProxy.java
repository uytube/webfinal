package servicios;

public class ListaEndpointProxy implements servicios.ListaEndpoint {
  private String _endpoint = null;
  private servicios.ListaEndpoint listaEndpoint = null;
  
  public ListaEndpointProxy() {
    _initListaEndpointProxy();
  }
  
  public ListaEndpointProxy(String endpoint) {
    _endpoint = endpoint;
    _initListaEndpointProxy();
  }
  
  private void _initListaEndpointProxy() {
    try {
      listaEndpoint = (new servicios.ListaEndpointServiceLocator()).getListaEndpointPort();
      if (listaEndpoint != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)listaEndpoint)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)listaEndpoint)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (listaEndpoint != null)
      ((javax.xml.rpc.Stub)listaEndpoint)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public servicios.ListaEndpoint getListaEndpoint() {
    if (listaEndpoint == null)
      _initListaEndpointProxy();
    return listaEndpoint;
  }
  
  public servicios.Video[] videosListaReproduccion(int arg0) throws java.rmi.RemoteException{
    if (listaEndpoint == null)
      _initListaEndpointProxy();
    return listaEndpoint.videosListaReproduccion(arg0);
  }
  
  public void postCrearLista(java.lang.String arg0, java.lang.String arg1, boolean arg2, java.lang.String arg3) throws java.rmi.RemoteException{
    if (listaEndpoint == null)
      _initListaEndpointProxy();
    listaEndpoint.postCrearLista(arg0, arg1, arg2, arg3);
  }
  
  public java.lang.String agregarVideo(int arg0, int arg1) throws java.rmi.RemoteException{
    if (listaEndpoint == null)
      _initListaEndpointProxy();
    return listaEndpoint.agregarVideo(arg0, arg1);
  }
  
  public servicios.Lista[] listarListas(java.lang.String arg0) throws java.rmi.RemoteException{
    if (listaEndpoint == null)
      _initListaEndpointProxy();
    return listaEndpoint.listarListas(arg0);
  }
  
  public java.lang.String quitarVideo(int arg0, int arg1) throws java.rmi.RemoteException{
    if (listaEndpoint == null)
      _initListaEndpointProxy();
    return listaEndpoint.quitarVideo(arg0, arg1);
  }
  
  public void modificarLista(int arg0, boolean arg1) throws java.rmi.RemoteException{
    if (listaEndpoint == null)
      _initListaEndpointProxy();
    listaEndpoint.modificarLista(arg0, arg1);
  }
  
  public servicios.Lista listaPorID(int arg0) throws java.rmi.RemoteException{
    if (listaEndpoint == null)
      _initListaEndpointProxy();
    return listaEndpoint.listaPorID(arg0);
  }
  
  
}