package servicios;

public class VideoEndpointProxy implements servicios.VideoEndpoint {
  private String _endpoint = null;
  private servicios.VideoEndpoint videoEndpoint = null;
  
  public VideoEndpointProxy() {
    _initVideoEndpointProxy();
  }
  
  public VideoEndpointProxy(String endpoint) {
    _endpoint = endpoint;
    _initVideoEndpointProxy();
  }
  
  private void _initVideoEndpointProxy() {
    try {
      videoEndpoint = (new servicios.VideoEndpointServiceLocator()).getVideoEndpointPort();
      if (videoEndpoint != null) {
        if (_endpoint != null)
          ((javax.xml.rpc.Stub)videoEndpoint)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
        else
          _endpoint = (String)((javax.xml.rpc.Stub)videoEndpoint)._getProperty("javax.xml.rpc.service.endpoint.address");
      }
      
    }
    catch (javax.xml.rpc.ServiceException serviceException) {}
  }
  
  public String getEndpoint() {
    return _endpoint;
  }
  
  public void setEndpoint(String endpoint) {
    _endpoint = endpoint;
    if (videoEndpoint != null)
      ((javax.xml.rpc.Stub)videoEndpoint)._setProperty("javax.xml.rpc.service.endpoint.address", _endpoint);
    
  }
  
  public servicios.VideoEndpoint getVideoEndpoint() {
    if (videoEndpoint == null)
      _initVideoEndpointProxy();
    return videoEndpoint;
  }
  
  public void modificarVideo(servicios.Video arg0) throws java.rmi.RemoteException{
    if (videoEndpoint == null)
      _initVideoEndpointProxy();
    videoEndpoint.modificarVideo(arg0);
  }
  
  public servicios.Video datosDelVideo(int arg0) throws java.rmi.RemoteException{
    if (videoEndpoint == null)
      _initVideoEndpointProxy();
    return videoEndpoint.datosDelVideo(arg0);
  }
  
  public servicios.Video[] videosUsuario(java.lang.String arg0) throws java.rmi.RemoteException{
    if (videoEndpoint == null)
      _initVideoEndpointProxy();
    return videoEndpoint.videosUsuario(arg0);
  }
  
  public servicios.Video[] videoPorCategoria(servicios.Categoria arg0) throws java.rmi.RemoteException{
    if (videoEndpoint == null)
      _initVideoEndpointProxy();
    return videoEndpoint.videoPorCategoria(arg0);
  }
  
  public servicios.Video[] obtenerVideosUsuario(java.lang.String arg0) throws java.rmi.RemoteException{
    if (videoEndpoint == null)
      _initVideoEndpointProxy();
    return videoEndpoint.obtenerVideosUsuario(arg0);
  }
  
  public void subirVideoYT(servicios.Video arg0, java.lang.String arg1, java.lang.String arg2) throws java.rmi.RemoteException{
    if (videoEndpoint == null)
      _initVideoEndpointProxy();
    videoEndpoint.subirVideoYT(arg0, arg1, arg2);
  }
  
  
}