package packageWeb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.rpc.ServiceException;

import servicios.WslCanal;
import servicios.WslCanalService;
import servicios.WslCanalServiceLocator;
import servicios.WslCategorias;
import servicios.WslCategoriasService;
import servicios.WslCategoriasServiceLocator;
import servicios.WslListasService;
import servicios.WslListasServiceLocator;
import servicios.WslUsuarios;
import servicios.WslUsuariosPortBindingStub;
import servicios.WslUsuariosService;
import servicios.WslUsuariosServiceLocator;
import servicios.WslVideos;
import servicios.WslVideosService;
import servicios.WslVideosServiceLocator;

/**
 * Servlet implementation class home
 */
public class home extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public home() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		WslCategoriasService servicioCategorias = new WslCategoriasServiceLocator();
		WslVideosService servicioVideos = new WslVideosServiceLocator();
		servicios.Video videos[] = null;
		servicios.Categoria categorias[] = null;

		HttpSession sessiones = request.getSession();

		/*
		 * Si el usuario esta logeado Cargar las listas
		 */

		// request.setAttribute(name, o) existe unicamente en ese request
		try {
			WslCategorias portCategoria = servicioCategorias.getWslCategoriasPort();
			categorias = portCategoria.listarCategorias();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		try {
			WslVideos portVideos = servicioVideos.getWslVideosPort();
			videos = portVideos.videosRandom();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		System.out.println(videos);
		sessiones.setAttribute("categorias", categorias);
		request.setAttribute("videos", videos);

		request.getRequestDispatcher("index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
