package packageWeb;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import servicios.Lista;
import servicios.ListaEndpoint;
import servicios.ListaEndpointService;
import servicios.ListaEndpointServiceLocator;

/**
 * Servlet implementation class modificarLista
 */
public class modificarLista extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ListaEndpoint ListaPort;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public modificarLista() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		int idLista = Integer.parseInt(request.getParameter("idListaRepro"));

		// port list
		ListaEndpointService ListaLocator = new ListaEndpointServiceLocator();
		try {
			ListaPort = ListaLocator.getListaEndpointPort();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Lista listaRep = ListaPort.listaPorID(idLista);
		request.setAttribute("listaModi", listaRep);
		request.getRequestDispatcher("modificarListaRepro.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		String idListaStr = request.getParameter("idLista");
		int idLista = Integer.parseInt(idListaStr);

		ListaEndpointService ListaLocator = new ListaEndpointServiceLocator();
		try {
			ListaPort = ListaLocator.getListaEndpointPort();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		ListaPort.modificarLista(idLista, Boolean.parseBoolean(request.getParameter("privacidad")));
		
			response.sendRedirect("listasReproduccion?idLista=" + idLista);


	}

}
