package packageWeb;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import servicios.CanalEndpoint;
import servicios.CanalEndpointService;
import servicios.CanalEndpointServiceLocator;
import servicios.Usuario;

/**
 * Servlet implementation class darDeBaja
 */
public class darDeBaja extends HttpServlet {
	private static final long serialVersionUID = 1L;
  //  private CanalEndpointService servicio;
	private CanalEndpoint CanalPort;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public darDeBaja() {
        super();

        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.sendRedirect("/web/home");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		// Canal
				CanalEndpointService canalLocator = new CanalEndpointServiceLocator();

				try {
					CanalPort = canalLocator.getCanalEndpointPort();
				} catch (ServiceException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
		String user_log = ((Usuario)request.getSession().getAttribute("usuarioLogueado")).getNickname();
		
		CanalPort.darDeBaja(user_log);
		response.sendRedirect("/web/logout");
		
	}

}
