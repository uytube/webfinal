package packageWeb;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import servicios.Ver;
import servicios.VerService;
import servicios.VerServiceLocator;

/**
 * Servlet implementation class agregarQuitar
 */
public class agregarQuitar extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	//private ListaController controllerLista;
	private VerService servicio;
	
    public agregarQuitar() {
        super();
        // TODO Auto-generated constructor stub
        //controllerLista = new ListaController();
        //endpoint
      	servicio = new VerServiceLocator();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		int idvid = Integer.parseInt((String) request.getParameter("id_video"));
		int idList = Integer.parseInt((String) request.getParameter("id_lista"));

		if (request.getParameter("accion") != null) {
			try {
				Ver puerto = this.servicio.getverPort();
				response.getWriter().print(puerto.agregarVideo(idvid, idList));
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else {
			try {
				Ver puerto = this.servicio.getverPort();
				response.getWriter().print(puerto.quitarVideo(idvid, idList));
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

}
