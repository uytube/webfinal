package packageWeb;

import java.io.IOException;
import java.util.Calendar;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import servicios.Ver;
import servicios.VerService;
import servicios.VerServiceLocator;


/**
 * Servlet implementation class comentar
 */
public class comentar extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
	
	private VerService servicio;

	
    public comentar() {
        super();
        // TODO Auto-generated constructor stub

      //endpoint
      		servicio = new VerServiceLocator();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		
		
		int id_video = Integer.parseInt(request.getParameter("id_video"));
		String comentario = (String) request.getParameter("comentario");
		int id_comentario = Integer.parseInt(request.getParameter("id_comentario"));
		
		servicios.Usuario usuario = (servicios.Usuario)request.getSession().getAttribute("usuarioLogueado");
		
		try {
			Ver puerto = this.servicio.getverPort();
			String msg = puerto.postComentarioVideo(id_video, comentario, id_comentario, usuario.getNickname());
			System.out.println(msg);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		response.getWriter().print("Listo!");
	}

}
