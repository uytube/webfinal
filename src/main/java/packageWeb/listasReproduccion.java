package packageWeb;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import servicios.CategoriaEndpoint;
import servicios.CategoriaEndpointService;
import servicios.CategoriaEndpointServiceLocator;
import servicios.Lista;
import servicios.ListaEndpoint;
import servicios.ListaEndpointService;
import servicios.ListaEndpointServiceLocator;
import servicios.Video;
import servicios.Categoria;

/**
 * Servlet implementation class listasReproduccion
 */
public class listasReproduccion extends HttpServlet {
	private static final long serialVersionUID = 1L;
	// private VideoEndpoint VideoPort;
	private ListaEndpoint ListaPort;
	private CategoriaEndpoint CategoriaPort;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public listasReproduccion() {
		super();
		// TODO Auto-generated constructor stub

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {

		CategoriaEndpointService CategoriaLocator = new CategoriaEndpointServiceLocator();

		try {
			CategoriaPort = CategoriaLocator.getCategoriaEndpointPort();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		Categoria[] categorias = CategoriaPort.listaDeCategorias();
		request.setAttribute("categorias", categorias);

		if (request.getSession().getAttribute("usuarioLogueado") != null) {

			int idLista = Integer.parseInt(request.getParameter("idLista"));

			ListaEndpointService ListaLocator = new ListaEndpointServiceLocator();

			try {
				ListaPort = ListaLocator.getListaEndpointPort();
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Lista listaRep = ListaPort.listaPorID(idLista);
			Video[] videos = ListaPort.videosListaReproduccion(idLista);

			request.setAttribute("nombreLista", listaRep.getNombre());
			request.setAttribute("listaModi", listaRep);
			request.setAttribute("listaRepro", videos);

			String privacidadVideo = "";
			if (listaRep.getPrivado()) {
				privacidadVideo = "Privada";
			} else {
				privacidadVideo = "Publica";
			}
			request.setAttribute("privacidad", privacidadVideo);
			request.setAttribute("idListaRepro", idLista);

			request.getRequestDispatcher("listaReproduccion.jsp").forward(request, response);

		} else {
			request.getRequestDispatcher("index.jsp").forward(request, response);
		}

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
