package packageWeb;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import com.google.gson.Gson;

import servicios.Usuario;
import servicios.WslUsuarios;
import servicios.WslUsuariosService;
import servicios.WslUsuariosServiceLocator;


/**
 * Servlet implementation class findByUsername
 */
public class findByNickname extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private WslUsuariosService usuarioPort;
    /**
     * @see HttpServlet#HttpServlet()
     */
	private Gson gson = new Gson();

    public findByNickname() {
        super();
        usuarioPort = new WslUsuariosServiceLocator();

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String nickname = (String)request.getParameter("nickname");
		PrintWriter out = response.getWriter();
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		WslUsuarios port;
		try {
			port = this.usuarioPort.getWslUsuariosPort();
			Usuario user = port.consultarUsuario(nickname);

			System.out.println(user);
			boolean exists;
			if(user!=null) {
				exists = true;
			}else {
				exists = false;
			}
			String userExists = this.gson.toJson(exists);
			out.print(userExists);
			out.flush();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
