package packageWeb;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import servicios.AbstractList;
import servicios.Ver;
import servicios.VerService;
import servicios.VerServiceLocator;
import servicios.WslUsuarios;
import servicios.WslUsuariosService;
import servicios.WslUsuariosServiceLocator;


/**
 * Servlet implementation class ver
 */
public class ver extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */

	private VerService servicio;
	
    public ver() {
        super();
        // TODO Auto-generated constructor stub

		//endpoint
		servicio = new VerServiceLocator();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		WslUsuariosService servicioUsuarios = new WslUsuariosServiceLocator();
		servicios.Usuario user = (servicios.Usuario) (request.getSession().getAttribute("usuarioLogueado"));
		String id_video = (String) request.getParameter("id_video").split("@")[1];
		

		try {
			Ver puerto = this.servicio.getverPort();
			
			//Categorias
			servicios.Categoria[] categorias = puerto.listaDeCategorias();
			request.setAttribute("categorias", categorias);
			
			//Video
			servicios.Video video = puerto.datosDelVideo(Integer.parseInt(id_video));
			
			if (request.getSession().getAttribute("usuarioLogueado") == null) {
				request.setAttribute("siguiendo", "false");
			} else {
				// OBTENER SI SIGUE EL USUARIO

				//ENDPOINT ( listCanalesSeguidos )
				if (this.loSigue(
						video.getCanal().getNombre(),
						puerto.canalesSeguidos(user.getNickname())
						)) {
					request.setAttribute("siguiendo", "true");
				} else {

					request.setAttribute("siguiendo", "false");
				}

			}
			if(request.getSession().getAttribute("usuarioLogueado")!=null) {
				WslUsuarios portUsuarios = servicioUsuarios.getWslUsuariosPort();
				portUsuarios.setHistorial(video, (servicios.Usuario)request.getSession().getAttribute("usuarioLogueado"));
			}

			/*
			 * Datos del video
			 * 
			 * Titulo
			 * 
			 * Canal
			 * 
			 * nickname (Para dejar de seguir o seguir)
			 * 
			 * Link
			 * 
			 * 
			 */

			request.setAttribute("titulo", video.getNombre());
			request.setAttribute("id_video", id_video);
			request.setAttribute("canal_nombre", video.getCanal().getNombre());
			request.setAttribute("nickname", video.getCanal().getUsuario().getNickname());
			String[] url = video.getUrl().split("/");
			request.setAttribute("link", url[url.length-1]);
			request.setAttribute("descripcion", video.getDescripcion());
			
			/*
			 * Valoracion del video
			 * */
			
			servicios.ValoracionVideo[] likes = puerto.valoracionesDeVideo(Integer.parseInt(id_video));
			//List<ValoracionVideo> likes = controllerValoracion.listaValoracionesVideo(Integer.parseInt(id_video));

			int like = 0;
			int dislike = 0;
			

			for(servicios.ValoracionVideo vv:likes) {
				if(vv.getValoracion()==1) {
					like++;
				}else {
					dislike++;
				}
			}
			
			//Si el usuario logeado ya le dio like
			request.setAttribute("likes", like);
			request.setAttribute("dislikes", dislike);
			
			//Si el usuario no esta logeado la accion es nula
			request.setAttribute("accion", "null");
			
			if(user != null) {
				
				try {
					servicios.ValoracionVideo val = puerto.devolverValoracionVideo(Integer.parseInt(id_video), user.getNickname());
					
					if(val.getValoracion()==1) {
						request.setAttribute("accion", "like");
					}else {
						request.setAttribute("accion", "dislike");
					}
					
					System.out.println("Existe valoracion");
					
				}catch(Exception e) {
					System.out.println("No existe valoracion del usuario");
				}
				
				//Actividad
				//controllerVideo.listaVideosUsuario(usuario_logueado);
				//controllerValoracion.listaValoracionesVideo()
				
				List<String> valoraciones = new ArrayList<String>();
				
				for(servicios.Video v:puerto.obtenerVideosUsuario(user.getNickname())) {
					for(servicios.ValoracionVideo val:puerto.valoracionesDeVideo(v.getId())) {
						if(val.getValoracion()==1) {
							valoraciones.add("A "+val.getUsuario().getNickname()+" le gusta "+v.getNombre());
						}else {
							valoraciones.add("A "+val.getUsuario().getNickname()+" no le gusta "+v.getNombre());
						}
					}
				}
				
				request.setAttribute("valoraciones", valoraciones);
				
			
				/*
				 * Si el usuario logeado es propietario del video, mostrar una lista con sus videos privados 
				 * */
				
				//Control de si es privado en jsp
				request.setAttribute("lista_videos", puerto.videosUsuario(user.getNickname()));
				
				
			}
			
			request.setAttribute("comentarios", puerto.comentariosDeUnVideo(video.getNombre()));

		} catch (ServiceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// Despachar
		request.getRequestDispatcher("verVideo.jsp").forward(request, response);
		
		
	} 

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}
	
	private boolean loSigue(String nombreCanal, servicios.Canal[] canales) {
		/*
		 * Si el usuario logeado sigue a String usuario Tabla relacionada a Seguir
		 * usuario
		 * 
		 * usuario_nickname | canal_nombre
		 */
		for (servicios.Canal c : canales) {
			if (c.getNombre().equals(nombreCanal)) {
				return true;
			}
		}
		return false;
	}

}
