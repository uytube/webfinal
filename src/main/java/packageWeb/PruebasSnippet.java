package packageWeb;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class PruebasSnippet
 */
public class PruebasSnippet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PruebasSnippet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		response.getWriter().append("Served at: ").append(request.getContextPath());
		request.getRequestDispatcher("PruebaJSSnippet.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String title = (String)request.getParameter("title");
		String duration = (String)request.getParameter("duration");
		String views = (String)request.getParameter("views");
		String url = (String)request.getParameter("url");
		System.out.println("El titulo del video es: "+title);
		System.out.println("Tiene una duracion de: "+duration);
		System.out.println("Y Tiene una "+views+" de reproducciones");
		System.out.println("Con unaa URL: "+url);
		request.getRequestDispatcher("PruebaJSSnippet.jsp").forward(request, response);
		
	}

}
