package packageWeb;

import java.io.File;
import java.io.IOException;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.rpc.ServiceException;

import servicios.Lista;
import servicios.ListaEndpoint;
import servicios.ListaEndpointService;
import servicios.ListaEndpointServiceLocator;
import servicios.Usuario;
import servicios.Video;


/**
 * Servlet implementation class categorias-celular
 */
public class listasparacel extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private ListaEndpoint ListaPort;

	/**
	 * @see HttpServlet#HttpServlet()
	 */

	public listasparacel() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		System.out.println("SER� QUE NO TA LOG?");
		if (request.getSession().getAttribute("usuarioLogueado") != null) {
			String nickname = (String) request.getAttribute("nickname");
			ListaEndpointService ListaLocator = new ListaEndpointServiceLocator();

			try {
				ListaPort = ListaLocator.getListaEndpointPort(); 
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			Lista[] listas = ListaPort.listarListas(nickname);
			
			System.out.println("MY USER IS: "+(((Usuario)request.getSession().getAttribute("usuarioLogueado")).getNickname()));
			
			
			request.setAttribute("listasUsuario", listas);
			request.getRequestDispatcher("listascelular.jsp").forward(request, response);
		} else {
			request.getRequestDispatcher("modalLogin.jsp").forward(request, response);
		}		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
