package packageWeb;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.output.*;

import servicios.ListaEndpoint;
import servicios.ListaEndpointService;
import servicios.ListaEndpointServiceLocator;
import servicios.VideoEndpoint;
import servicios.VideoEndpointService;
import servicios.VideoEndpointServiceLocator;
import servicios.WslCanal;
import servicios.WslCanalService;
import servicios.WslCanalServiceLocator;
import servicios.WslCategorias;
import servicios.WslCategoriasService;
import servicios.WslCategoriasServiceLocator;
import servicios.WslListas;
import servicios.WslListasService;
import servicios.WslListasServiceLocator;
import servicios.WslUsuarios;
import servicios.WslUsuariosService;
import servicios.WslUsuariosServiceLocator;
import servicios.WslVideos;
import servicios.WslVideosService;
import servicios.WslVideosServiceLocator;

import java.io.File;
import java.io.IOException;
import java.rmi.RemoteException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.rpc.ServiceException;


/**
 * Servlet implementation class canal
 */
public class canal extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	private boolean isMultipart;
	private String filePath;
	private int maxFileSize = 10000 * 1024;
	private int maxMemSize = 4 * 1024;
	private File file;


	public canal() {
		super();
	
	}

	public void init() {
		// Get the file location where it would be stored.
		filePath = getServletContext().getInitParameter("file-upload");
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// categorias
		// listas

		WslUsuariosService servicioUsuarios = new WslUsuariosServiceLocator();
		WslVideosService servicioVideos = new WslVideosServiceLocator();
		WslCanalService servicioCanal = new WslCanalServiceLocator();
		WslListasService servicioListas = new WslListasServiceLocator();
		ListaEndpointService ListaLocator = new ListaEndpointServiceLocator();
		ListaEndpoint ListaPort = null;

		WslCategoriasService servicioCategorias = new WslCategoriasServiceLocator();
		servicios.Usuario user = null;
		servicios.Usuario seguidores[] = null;
		servicios.Canal seguidos[] = null;
		servicios.Canal canal = null;
		servicios.Video videos[] = null;
		servicios.Lista listas[];
		servicios.Categoria categorias[] = null;
		servicios.Historial historial[] = null;
		try {
			WslCategorias portCategoria = servicioCategorias.getWslCategoriasPort();
			categorias = portCategoria.listarCategorias();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		VideoEndpoint VideoPort = null;
		// Video
		VideoEndpointService videoLocator = new VideoEndpointServiceLocator();
		try {
			VideoPort = videoLocator.getVideoEndpointPort();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			WslCanal portCanal = servicioCanal.getWslCanalPort();
			canal = portCanal.obtenerCanalUsuario(request.getParameter("canal"));
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		//videos = portVideos.listaVideosUsuario((String) canal.getUsuario().getNickname());
		videos = VideoPort.obtenerVideosUsuario((String)canal.getUsuario().getNickname());


		try {
			WslUsuarios portUsuarios = servicioUsuarios.getWslUsuariosPort();
			user = portUsuarios.consultarUsuario((String) canal.getUsuario().getNickname());
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			WslVideos portVideos = servicioVideos.getWslVideosPort();
			videos = portVideos.listaVideosUsuario((String) canal.getUsuario().getNickname());
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			WslUsuarios portUsuarios = servicioUsuarios.getWslUsuariosPort();
			historial = portUsuarios.getHistorial((String) canal.getUsuario().getNickname());
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			ListaPort = ListaLocator.getListaEndpointPort();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// listas = ListaPort.listarListas((String)canal.getUsuario().getNickname());
		listas = ListaPort.listarListas((String) canal.getUsuario().getNickname());
		try {
			WslUsuarios portUsuarios = servicioUsuarios.getWslUsuariosPort();
			seguidos = portUsuarios.listCanalesSeguidos((String) canal.getUsuario().getNickname());
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		try {
			WslUsuarios portUsuarios = servicioUsuarios.getWslUsuariosPort();
			seguidores = portUsuarios.listUsuariosSeguidores((String) canal.getUsuario().getNickname());
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		request.setAttribute("categorias", categorias);
		request.setAttribute("canal", canal);
		request.setAttribute("usuario", user);
		request.setAttribute("listas", listas);
		request.setAttribute("listaVideos", videos);
		request.setAttribute("seguidos", seguidos);
		request.setAttribute("seguidores", seguidores);
		request.setAttribute("duenioCanal", "false");
		request.setAttribute("historial", historial);
		System.out.println(request.getSession().getAttribute("usuarioLogueado"));
		if (request.getSession().getAttribute("usuarioLogueado") != null) {
			if (((servicios.Usuario) request.getSession().getAttribute("usuarioLogueado")).getNickname()
					.equals(canal.getUsuario().getNickname())) {
				request.setAttribute("duenioCanal", "true");
			}
			request.setAttribute("logueado",
					((servicios.Usuario) request.getSession().getAttribute("usuarioLogueado")).getNickname());
		}

		System.out.println(request.getSession().getAttribute("usuarioLogueado"));
		if (request.getSession().getAttribute("usuarioLogueado") == null) {
			request.setAttribute("siguiendo", "false");
		} else {
			// OBTENER SI SIGUE EL USUARIO
			if (this.loSigue(canal.getNombre(),
					((servicios.Usuario) request.getSession().getAttribute("usuarioLogueado")).getNickname())) {
				request.setAttribute("siguiendo", "true");
			} else {

				request.setAttribute("siguiendo", "false");
			}

		}

		request.getRequestDispatcher("canal/index.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// Check that we have a file upload request
		WslCanalService servicioCanal = new WslCanalServiceLocator();
		String canalName = "";
		String canalDescription = "";
		boolean canalPrivacity = false;
		String nickname = "";
		String password = "";
		String nombre = "";
		String apellido = "";
		String correo = "";
		Date fdate = new Date();
		servicios.Canal canal = null;
		byte[] img = null;
		isMultipart = ServletFileUpload.isMultipartContent(request);
		response.setContentType("text/html");
		java.io.PrintWriter out = response.getWriter();

		if (!isMultipart) {
			return;
		}

		DiskFileItemFactory factory = new DiskFileItemFactory();

		// maximum size that will be stored in memory
		factory.setSizeThreshold(maxMemSize);

		// Location to save data that is larger than maxMemSize.
		// factory.setRepository(new File("c:\\temp"));

		// Create a new file upload handler
		ServletFileUpload upload = new ServletFileUpload(factory);

		// maximum file size to be uploaded.
		upload.setSizeMax(maxFileSize);
		String fileName = "";
		try {
			// Parse the request to get file items.
			List<FileItem> fileItems = upload.parseRequest(request);

			// Process the uploaded file items
			Iterator i = fileItems.iterator();
			for (FileItem fi : fileItems) {
				if (!fi.isFormField()) {
					// Get the uploaded file parameters
					String fieldName = fi.getFieldName();
					fileName = fi.getName();
					String contentType = fi.getContentType();
					boolean isInMemory = fi.isInMemory();
					long sizeInBytes = fi.getSize();
					// Write the file
					System.out.println(filePath);
					if (fileName.lastIndexOf("\\") >= 0) {
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\")));
					} else {
						file = new File(filePath + fileName.substring(fileName.lastIndexOf("\\") + 1));
					}
					img = fi.get();
				} else {
					String fieldname = fi.getFieldName();
					String fieldvalue = fi.getString();
					if (fieldname.equals("canalName")) {
						canalName = fieldvalue;
					} else if (fieldname.equals("canalDescription")) {
						canalDescription = fieldvalue;
					} else if (fieldname.equals("canalPrivacity")) {
						canalPrivacity = (fieldvalue == "true") ? true : false;
					} else if (fieldname.equals("nickname")) {
						nickname = fieldvalue;
					} else if (fieldname.equals("password")) {
						System.out.println("aca");
						password = fieldvalue;
					} else if (fieldname.equals("nombre")) {
						nombre = fieldvalue;
					} else if (fieldname.equals("apellido")) {
						apellido = fieldvalue;
					} else if (fieldname.equals("correo")) {
						correo = fieldvalue;
					} else if (fieldname.equals("fdate")) {
						try {
							fdate = new SimpleDateFormat("yyyy-MM-dd").parse((String) fieldvalue);
						} catch (ParseException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}

				}
			}
		} catch (Exception ex) {
			System.out.println(ex);
		}
		password = (password!="")? password:((servicios.Usuario) request.getSession().getAttribute("usuarioLogueado")).getPassword();
		servicios.Usuario user = (servicios.Usuario) request.getSession().getAttribute("usuarioLogueado");
		user.setNickname(nickname);
		user.setNombre(nombre);
		user.setCorreo(correo);
		user.setFnacimiento(fdate);
		user.setApellido(apellido);
		user.setPassword(password);
		if (fileName != "") {
			user.setImg(fileName);
		}
		if (password != "") {

		}
		try {
			WslCanal portCanal = servicioCanal.getWslCanalPort();
			canal = portCanal.obtenerCanalUsuario(nickname);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		canal.setDescripcion(canalDescription);
		try {
			WslCanal portCanal = servicioCanal.getWslCanalPort();
			portCanal.actualizarCanal(canal);
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		WslUsuariosService servicio = new WslUsuariosServiceLocator();

		try {
			WslUsuarios port = servicio.getWslUsuariosPort();
			port.modificarUsuario(user, img, fileName);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		HttpSession sesion = request.getSession();
		sesion.setAttribute("usuarioLogueado", user);
		response.sendRedirect("/web/canal?canal=" + user.getNickname());
	}

	private boolean loSigue(String nombreCanal, String nickname) throws RemoteException {
		/*
		 * Si el usuario logeado sigue a String usuario Tabla relacionada a Seguir
		 * usuario
		 * 
		 * usuario_nickname | canal_nombre
		 */
		WslUsuariosService servicioUsuarios = new WslUsuariosServiceLocator();
		servicios.Canal canales[] = null;
		try {
			WslUsuarios portUsuarios = servicioUsuarios.getWslUsuariosPort();
			canales = portUsuarios.listCanalesSeguidos(nickname);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (canales != null) {
			for (servicios.Canal c : canales) {
				if (c.getNombre().equals(nombreCanal)) {
					return true;
				}
			}
		}
		return false;
	}

}
