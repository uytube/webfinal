package packageWeb;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import servicios.CategoriaEndpoint;
import servicios.CategoriaEndpointService;
import servicios.CategoriaEndpointServiceLocator;
import servicios.Video;
import servicios.VideoEndpoint;
import servicios.VideoEndpointService;
import servicios.VideoEndpointServiceLocator;
import servicios.WslCategorias;
import servicios.WslCategoriasService;
import servicios.WslCategoriasServiceLocator;
import servicios.WslVideosService;
import servicios.WslVideosServiceLocator;

/**
 * Servlet implementation class modificarVideo
 */
public class modificarVideo extends HttpServlet {
	private static final long serialVersionUID = 1L;
   
	private VideoEndpointService servicioVideo;
	private CategoriaEndpointService servicioCategoria;
    /**
     * @see HttpServlet#HttpServlet()
     */
    public modificarVideo() {
        super();
        // TODO Auto-generated constructor stub

        servicioVideo = new VideoEndpointServiceLocator();
        servicioCategoria = new CategoriaEndpointServiceLocator();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		WslCategoriasService servicioCategorias = new WslCategoriasServiceLocator();
		servicios.Categoria categorias[] = null;

		VideoEndpoint portVideo;
		try {
			WslCategorias portCategoria = servicioCategorias.getWslCategoriasPort();
			categorias = portCategoria.listarCategorias();
		} catch (ServiceException e) {
			e.printStackTrace();
		}

		try {
			portVideo = this.servicioVideo.getVideoEndpointPort();
			// obtenemos usuario
			servicios.Usuario user = (servicios.Usuario)request.getSession().getAttribute("usuarioLogueado");
			//agregamos usuario al request
			request.setAttribute("Usuario", user.getNickname());
			// agregamos video al request
			String id_video = (String) request.getParameter("id_video");
			Video video =  portVideo.datosDelVideo(Integer.parseInt(id_video));
			//ControllerVideo.consultaVideoPorID(Integer.parseInt(id_video));
			//Video video = ControllerVideo.consultaVideo("Show de goles44", usuarioLogueado);
			request.setAttribute("Video", video);
			request.setAttribute("categorias", categorias);
			request.getRequestDispatcher("ModificarVideo.jsp").forward(request, response);
			
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		VideoEndpoint portVideo;
		CategoriaEndpoint portCategoria;
		
		try {
			portVideo = this.servicioVideo.getVideoEndpointPort();
			portCategoria = this.servicioCategoria.getCategoriaEndpointPort();
			servicios.Categoria cat = portCategoria.obtenerCategoria((String)request.getParameter("categoria"));
			servicios.Usuario user = (servicios.Usuario)request.getSession().getAttribute("usuarioLogueado"); //recibido por models.
			//controladorCategoria.obtenerCategoria((String)request.getParameter("categoria"));
			String nombr=(String)request.getParameter("nombre");
			String desc=(String)request.getParameter("descripcion");
			//String dur=(String)request.getParameter("duracion");
			System.out.println(request.getParameter("id"));
			Video Vid = portVideo.datosDelVideo(Integer.parseInt(request.getParameter("id"))); 
			//(Video)ControllerVideo.consultaVideoPorID(Integer.parseInt(request.getParameter("id")));
			Vid.setNombre(nombr);
			Vid.setDescripcion(desc);
			//Vid.setDuracion(dur);
			//Vid.setFecha(java.sql.Date.valueOf(LocalDate.now()));
			//Vid.setFecha(java.util.Calendar.getInstance());
			Vid.setCategoria(cat);
			Boolean Privacity;
			if (request.getParameter("privacidad").equals("true")) {
				 Privacity = true;
			} 
			else { Privacity = false;}
			
			Vid.setEs_publico(Privacity);
			//Vid.setUrl((String)request.getParameter("url"));
			//ControllerVideo.modificarVideo(Vid);
			portVideo.modificarVideo(Vid);
			
			response.sendRedirect("/web/canal?canal=" +user.getNickname());
			//response.sendRedirect("/web/modificarVideo?id_video=" + Vid.getId());
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
