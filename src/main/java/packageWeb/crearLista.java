package packageWeb;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.rpc.ServiceException;

import servicios.Ver;
import servicios.Usuario;
import servicios.VerService;
import servicios.VerServiceLocator;

/**
 * Servlet implementation class crearLista
 */


public class crearLista extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */

	private VerService servicio;

    public crearLista() {
        super();
        // TODO Auto-generated constructor stub
		servicio = new VerServiceLocator();

    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		HttpSession sesion = request.getSession();
		
		if (sesion != null) {
			Ver puerto;
			try {
				puerto = this.servicio.getverPort();
				servicios.Lista[] listas = puerto.listarListas(((Usuario)sesion.getAttribute("usuarioLogueado")).getNickname());		
				request.getSession().setAttribute("listas",listas);
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		response.sendRedirect("home");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String nombre_lista = (String)request.getParameter("nombre_lista");
		
		String categoria = (String)request.getParameter("categoria");
		System.out.println(categoria);
		
		//Privado chequeado = on else null
		boolean privado;
		if(request.getParameter("privado") != null) {
			privado = true;
		}else {
			privado = false;
		}
		
		String user_logueado = ((Usuario)request.getSession().getAttribute("usuarioLogueado")).getNickname();
		
		try {
			Ver puerto = this.servicio.getverPort();
			puerto.postCrearLista(nombre_lista, categoria, privado, user_logueado);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		doGet(request, response);
	}

}
