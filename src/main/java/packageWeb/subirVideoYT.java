package packageWeb;

import java.io.IOException;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.swing.JOptionPane;
import javax.xml.rpc.ServiceException;

import servicios.CanalEndpoint;
import servicios.CanalEndpointService;
import servicios.CanalEndpointServiceLocator;
import servicios.Categoria;
import servicios.CategoriaEndpoint;
import servicios.CategoriaEndpointService;
import servicios.CategoriaEndpointServiceLocator;
//import servicios.Usuario;
import servicios.Video;
import servicios.VideoEndpoint;
import servicios.VideoEndpointService;
import servicios.VideoEndpointServiceLocator;

/**
 * Servlet implementation class subirVideo
 */
public class subirVideoYT extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	private Date fechaPub;
	private VideoEndpoint VideoPort;
	private CanalEndpoint CanalPort;
	private CategoriaEndpoint CategoriaPort;

	public subirVideoYT() {
		super();

		// TODO Auto-generated constructor stub
	}

	/**
	 * 
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		if (request.getSession().getAttribute("usuarioLogueado") != null) {
			// Categoria
			CategoriaEndpointService CategoriaLocator = new CategoriaEndpointServiceLocator();

			try {
				CategoriaPort = CategoriaLocator.getCategoriaEndpointPort();
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

			Categoria[] categorias = CategoriaPort.listaDeCategorias();

			request.setAttribute("categorias", categorias);
				request.getRequestDispatcher("uploadVideo.jsp").forward(request, response); 
		} else {
			//request.getRequestDispatcher("index.jsp").forward(request, response);
			response.sendRedirect("/web/home");
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		// doGet(request, response);

		// iniciamos los puertos
		// Canal
		CanalEndpointService canalLocator = new CanalEndpointServiceLocator();

		try {
			CanalPort = canalLocator.getCanalEndpointPort();
		} catch (ServiceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// Video
		VideoEndpointService videoLocator = new VideoEndpointServiceLocator();

		try {
			VideoPort = videoLocator.getVideoEndpointPort();
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		CategoriaEndpointService categoriaLocator = new CategoriaEndpointServiceLocator();

		// Categoria
		try {
			CategoriaPort = categoriaLocator.getCategoriaEndpointPort();
		} catch (ServiceException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		// variables recibidas
		String nombre_video = request.getParameter("title");
		String descripcion = request.getParameter("descripcion");
		String url_video = request.getParameter("url");
		String categoria = request.getParameter("categoria");
		String privacidad = request.getParameter("privacidad");
		String duracion = request.getParameter("duracion");
		String visitas = request.getParameter("views");
		String user_log = ((servicios.Usuario) request.getSession().getAttribute("usuarioLogueado")).getNickname();
		// fecha del video
		Calendar cal = new GregorianCalendar();
		cal.getTime();
		// categoria
		if (categoria.contentEquals("Sin Categoria")) {
			categoria = CategoriaPort.obtenerCategoriaPorID(13).getNombre();
		} else {
			categoria = CategoriaPort.obtenerCategoriaPorID(Integer.parseInt(categoria)).getNombre();
		}
		// duracion del video
		duracion = duracion.substring(2); // le saque el pt
		duracion = duracion.replace("S", ""); // reemplazo si existen
		duracion = duracion.replace("M", ":");
		duracion = duracion.replace("H", ":");

		if (duracion.length() == 2) { // XXS
			duracion = duracion.substring(0, 2);
			duracion = "0:" + duracion; // formato YT 0:XX
		} else if (duracion.length() <= 4
				&& (duracion.substring(duracion.indexOf(":"), duracion.length()).length() != 3)) {
			duracion = duracion.replace(":", ":0");
		} else if (duracion.length() >= 5) { // ZZH-XXM-YYS
			int separador = duracion.indexOf(":");
			if (duracion.charAt(separador) == duracion.charAt(separador + 2)) {
				duracion = duracion.replace(":", ":0");
			}
		}

		// cargo video
		Video v = new Video();
		v.setNombre(nombre_video);
		v.setDescripcion(descripcion);
		v.setFecha(cal);
		v.setUrl("https://youtu.be/" + url_video);
		System.out.println("PRIVACIDAD VIDEO: "+privacidad);
		Boolean PrivacityVideo;
		if (privacidad.equals("publico")) {
			PrivacityVideo = true;
		} 
		else { PrivacityVideo = false;}
		v.setEs_publico(PrivacityVideo);
		v.setDuracion(duracion);
		v.setVisitas(visitas);
		v.setCanal(CanalPort.obtenerCanalUsuario(user_log));
		VideoPort.subirVideoYT(v, user_log, categoria);
		response.sendRedirect("/web/home");

	}
}
