package packageWeb;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.rpc.ServiceException;

import servicios.ListaEndpoint;
import servicios.ListaEndpointService;
import servicios.ListaEndpointServiceLocator;
import servicios.WslListas;
import servicios.WslListasService;
import servicios.WslListasServiceLocator;
import servicios.WslUsuarios;
import servicios.WslUsuariosService;
import servicios.WslUsuariosServiceLocator;

/**
 * Servlet implementation class login
 */
public class login extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */

    public login() {
        super();
        // TODO Auto-generated constructor stub

        
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//Listas de kairoh
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		ListaEndpointService servicioListas = new ListaEndpointServiceLocator();
		servicios.Lista listas[] = null;
		String nickname = (String)request.getParameter("user");
		String password = (String)request.getParameter("password");
		System.out.println(nickname+" "+password);
		WslUsuariosService servicio = new WslUsuariosServiceLocator();
		servicios.Usuario user = null;
		
		try {
			WslUsuarios port =  servicio.getWslUsuariosPort();
			user = port.login(nickname, password);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		if (user != null) {
			HttpSession sesion = request.getSession();
			sesion.setAttribute("usuarioLogueado", user);
			
			try {
				//9877
				ListaEndpoint portListas =  servicioListas.getListaEndpointPort();
				listas = portListas.listarListas(user.getNickname());
				sesion.setAttribute("listas",listas);
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		response.sendRedirect("/web/home");
	}

}
