package packageWeb;
import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import servicios.WslCanal;
import servicios.WslCanalService;
import servicios.WslCanalServiceLocator;
import servicios.WslCategorias;
import servicios.WslCategoriasService;
import servicios.WslCategoriasServiceLocator;
import servicios.WslListas;
import servicios.WslListasService;
import servicios.WslListasServiceLocator;
import servicios.WslUsuarios;
import servicios.WslUsuariosService;
import servicios.WslUsuariosServiceLocator;
import servicios.WslVideos;
import servicios.WslVideosService;
import servicios.WslVideosServiceLocator;

/**
 * Servlet implementation class search
 */
public class search extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */

    public search() {
        super();

    }
    
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		WslVideosService servicioVideos = new WslVideosServiceLocator();
		WslCanalService servicioCanal = new WslCanalServiceLocator();
		WslListasService servicioListas = new WslListasServiceLocator();
		WslCategoriasService servicioCategorias = new WslCategoriasServiceLocator();
		servicios.Canal canales[] = null;
		servicios.Video videos[] = null;
		servicios.Lista listas[] = null;
		servicios.Categoria categorias[] = null;
		
		String search = (String)request.getParameter("q");
		String fecha = (String)request.getParameter("fecha");
		String alfabeticamente = (String)request.getParameter("alfabeticamente");
		
		try {
			WslCanal portCanal =  servicioCanal.getWslCanalPort();
			canales = portCanal.searchCanales(search, alfabeticamente);
		} catch (ServiceException e) {
			e.printStackTrace();
		}


		
		try {
			WslVideos portVideos =  servicioVideos.getWslVideosPort();
			videos = portVideos.searchVideo(search, fecha, alfabeticamente);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		try {
			WslListas portListas =  servicioListas.getWslListasPort();
			listas = portListas.searchListas(search, alfabeticamente);
		} catch (ServiceException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		try {
			WslCategorias portCategoria = servicioCategorias.getWslCategoriasPort();
			categorias = portCategoria.listarCategorias();
		} catch (ServiceException e) {
			e.printStackTrace();
		}
		
		System.out.println(canales);
		request.setAttribute("categorias", categorias);
		request.setAttribute("listaVideos", videos);
		request.setAttribute("listaCanales", canales);
		request.setAttribute("listaListas", listas);
		request.getRequestDispatcher("search.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
