package packageWeb;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import servicios.Categoria;
import servicios.CategoriaEndpoint;
import servicios.CategoriaEndpointService;
import servicios.CategoriaEndpointServiceLocator;
import servicios.Video;
import servicios.VideoEndpoint;
import servicios.VideoEndpointService;
import servicios.VideoEndpointServiceLocator;

/**
 * Servlet implementation class categorias
 */
public class categorias extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CategoriaEndpoint CategoriaPort;
	private VideoEndpoint VideoPort;
	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public categorias() {
		super();
		// TODO Auto-generated constructor stub
	}
	

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

		// Categoria
			CategoriaEndpointService CategoriaLocator = new CategoriaEndpointServiceLocator();
			try {
				CategoriaPort = CategoriaLocator.getCategoriaEndpointPort();
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		// Video
			VideoEndpointService videoLocator = new VideoEndpointServiceLocator();
			try {
				VideoPort = videoLocator.getVideoEndpointPort();
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		String nombreCategoria = (String) request.getParameter("nombreCategoria");
		
		Video [] videos = VideoPort.videoPorCategoria(CategoriaPort.obtenerCategoria(nombreCategoria));		
		request.setAttribute("Categoria", nombreCategoria);
		request.setAttribute("listaVideos", videos);

		// Despachar
		request.getRequestDispatcher("categorias.jsp").forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
