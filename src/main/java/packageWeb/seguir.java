package packageWeb;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.rpc.ServiceException;

import servicios.Usuario;
import servicios.Ver;
import servicios.VerService;
import servicios.VerServiceLocator;


/**
 * Servlet implementation class seguir
 */
public class seguir extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */

	private VerService servicio;
	
	public seguir() {
		super();

		servicio = new VerServiceLocator();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
	
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String usuario_logeado = ((Usuario)request.getSession().getAttribute("usuarioLogueado")).getNickname();
		
		//Testing AJAX 
		System.out.println("Ajax por post");
		System.out.println("User: "+usuario_logeado);
		//El estado de la subscripcion
		// true o false
		String subscribe = (String)request.getParameter("subscribe");
		System.out.println(subscribe);
		
		//nombre del usuario a Seguir
		String nickname = (String)request.getParameter("nickname");
		System.out.println(nickname);
		
		//el nombre del canal a dejar de seguir
		String canal_nombre = (String)request.getParameter("canal_nombre");
		System.out.println(canal_nombre);
		
		response.setContentType("text/plain");
		
		if(subscribe.equals("true")) {
			
			try {
				Ver port = servicio.getverPort();
				port.postSeguirUsuario(usuario_logeado,nickname);
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//controllerUser.seguirUsuario(usuario_logeado, nickname);
			response.getWriter().print("Ahora sigues a "+nickname);
		}else {
			try {
				Ver port = servicio.getverPort();
				port.postDejarSeguirUsuario(usuario_logeado, canal_nombre);
			} catch (ServiceException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			//controllerUser.dejarDeSeguir(usuario_logeado, canal_nombre);
			response.getWriter().print("Dejaste de seguir a "+canal_nombre);
		}
		
		
		
	}

}
