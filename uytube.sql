-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 24-11-2019 a las 02:23:42
-- Versión del servidor: 10.4.8-MariaDB
-- Versión de PHP: 7.1.33

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `uytube`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `canal`
--

CREATE TABLE `canal` (
  `nombre` varchar(255) NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `privacidad` bit(1) DEFAULT NULL,
  `usuario_nickname` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `canal`
--

INSERT INTO `canal` (`nombre`, `deleted`, `descripcion`, `privacidad`, `usuario_nickname`) VALUES
('cachilas', b'0', 'Canal de Cachilas', b'1', 'cachilas'),
('chino', b'0', 'Canal del 10T', b'1', 'chino'),
('cristy257', b'0', 'HOLA?!', b'0', 'crisy257'),
('diegop', b'0', 'Canal de DiegoP', b'1', 'diegop'),
('emii22', b'0', 'asd', b'0', 'emito'),
('emiib', b'0', 'Coso', b'1', 'emiib'),
('hectorg', b'0', 'Canal de Héctor Guido', b'1', 'hectorg'),
('hrubino', b'0', 'Canal de HR', b'1', 'hrubino'),
('juliob', b'0', 'Canal de Julito', b'1', 'juliob'),
('kairoh', b'0', 'Canal de KairoH', b'1', 'kairoh'),
('marcelot', b'0', 'El canal de un grande, Marcelito.', b'1', 'marcelot'),
('mbusca', b'0', 'Canal de Martin Buscaglia', b'1', 'mbusca'),
('nicoJ', b'0', 'Canal de Nicito', b'1', 'nicoJ'),
('novick', b'0', 'Canal de NV', b'1', 'novick'),
('robinh', b'0', 'Robin\'s Canal', b'1', 'robinh'),
('sergiop', b'0', 'Que rica cocina con sergiop.', b'1', 'sergiop'),
('tabarec', b'0', 'Canal del Taba', b'1', 'tabarec'),
('tonyp', b'0', 'Canal del 10P', b'1', 'tonyp');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `categoria`
--

CREATE TABLE `categoria` (
  `id` int(11) NOT NULL,
  `nombre` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `categoria`
--

INSERT INTO `categoria` (`id`, `nombre`) VALUES
(1, 'Musica'),
(2, 'Deporte'),
(3, 'Carnaval'),
(4, 'Noticias'),
(5, 'Entretenimiento'),
(6, 'Comida'),
(7, 'Videojuego'),
(8, 'Ciencia y tecnologia'),
(9, 'ONG y activismo'),
(10, 'Gente y blogs'),
(11, 'Mascotas y animales'),
(12, 'Viajes y eventos'),
(13, 'Sin Categoria');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario`
--

CREATE TABLE `comentario` (
  `id` bigint(20) NOT NULL,
  `comentario` varchar(255) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `usuario_nickname` varchar(255) DEFAULT NULL,
  `vid_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `comentario_comentario`
--

CREATE TABLE `comentario_comentario` (
  `Comentario_id` bigint(20) NOT NULL,
  `respuestas_id` bigint(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(87),
(87),
(87),
(87);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lista`
--

CREATE TABLE `lista` (
  `id` int(11) NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `nombre_lista` varchar(255) DEFAULT NULL,
  `privado` bit(1) DEFAULT NULL,
  `tipo` bit(1) DEFAULT NULL,
  `canal_nombre` varchar(255) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `lista`
--

INSERT INTO `lista` (`id`, `deleted`, `nombre_lista`, `privado`, `tipo`, `canal_nombre`, `categoria_id`) VALUES
(2, b'0', 'Escuchar mas tarde', b'1', b'0', 'emii22', 13),
(3, b'0', 'Deporte total', b'1', b'0', 'emii22', 13),
(4, b'0', 'Novedades generales', b'1', b'0', 'emii22', 13),
(8, b'0', 'Escuchar mas tarde', b'1', b'0', 'cachilas', 13),
(9, b'0', 'Deporte total', b'1', b'0', 'cachilas', 13),
(10, b'0', 'Novedades generales', b'1', b'0', 'cachilas', 13),
(11, b'0', 'Escuchar mas tarde', b'1', b'0', 'hrubino', 13),
(12, b'0', 'Deporte total', b'1', b'0', 'hrubino', 13),
(13, b'0', 'Novedades generales', b'1', b'0', 'hrubino', 13),
(14, b'0', 'Escuchar mas tarde', b'1', b'0', 'mbusca', 13),
(15, b'0', 'Deporte total', b'1', b'0', 'mbusca', 13),
(16, b'0', 'Novedades generales', b'1', b'0', 'mbusca', 13),
(17, b'0', 'Escuchar mas tarde', b'1', b'0', 'hectorg', 13),
(18, b'0', 'Deporte total', b'1', b'0', 'hectorg', 13),
(19, b'0', 'Novedades generales', b'1', b'0', 'hectorg', 13),
(20, b'0', 'Escuchar mas tarde', b'1', b'0', 'tabarec', 13),
(21, b'0', 'Deporte total', b'1', b'0', 'tabarec', 13),
(22, b'0', 'Novedades generales', b'1', b'0', 'tabarec', 13),
(23, b'0', 'Escuchar mas tarde', b'1', b'0', 'juliob', 13),
(24, b'0', 'Deporte total', b'1', b'0', 'juliob', 13),
(25, b'0', 'Novedades generales', b'1', b'0', 'juliob', 13),
(26, b'0', 'Escuchar mas tarde', b'1', b'0', 'diegop', 13),
(27, b'0', 'Deporte total', b'1', b'0', 'diegop', 13),
(28, b'0', 'Novedades generales', b'1', b'0', 'diegop', 13),
(29, b'0', 'Escuchar mas tarde', b'1', b'0', 'kairoh', 13),
(30, b'0', 'Deporte total', b'1', b'0', 'kairoh', 13),
(31, b'0', 'Novedades generales', b'1', b'0', 'kairoh', 13),
(32, b'0', 'Escuchar mas tarde', b'1', b'0', 'robinh', 13),
(33, b'0', 'Deporte total', b'1', b'0', 'robinh', 13),
(34, b'0', 'Novedades generales', b'1', b'0', 'robinh', 13),
(35, b'0', 'Escuchar mas tarde', b'1', b'0', 'marcelot', 13),
(36, b'0', 'Deporte total', b'1', b'0', 'marcelot', 13),
(37, b'0', 'Novedades generales', b'1', b'0', 'marcelot', 13),
(38, b'0', 'Escuchar mas tarde', b'1', b'0', 'novick', 13),
(39, b'0', 'Deporte total', b'1', b'0', 'novick', 13),
(40, b'0', 'Novedades generales', b'1', b'0', 'novick', 13),
(41, b'0', 'Escuchar mas tarde', b'1', b'0', 'sergiop', 13),
(42, b'0', 'Deporte total', b'1', b'0', 'sergiop', 13),
(43, b'0', 'Novedades generales', b'1', b'0', 'sergiop', 13),
(44, b'0', 'Escuchar mas tarde', b'1', b'0', 'chino', 13),
(45, b'0', 'Deporte total', b'1', b'0', 'chino', 13),
(46, b'0', 'Novedades generales', b'1', b'0', 'chino', 13),
(47, b'0', 'Escuchar mas tarde', b'1', b'0', 'tonyp', 13),
(48, b'0', 'Deporte total', b'1', b'0', 'tonyp', 13),
(49, b'0', 'Novedades generales', b'1', b'0', 'tonyp', 13),
(50, b'0', 'Escuchar mas tarde', b'1', b'0', 'nicoJ', 13),
(51, b'0', 'Deporte total', b'1', b'0', 'nicoJ', 13),
(52, b'0', 'Novedades generales', b'1', b'0', 'nicoJ', 13),
(73, b'0', 'De fiesta', b'1', b'0', 'tabarec', 13),
(74, b'0', 'Nostalgia', b'0', b'0', 'kairoh', 13),
(75, b'0', 'Novedades FING', b'0', b'0', 'hectorg', 13),
(76, b'0', 'De todo un poco', b'1', b'0', 'cachilas', 13),
(77, b'0', 'Noticias y CYT', b'0', b'0', 'nicoJ', 13),
(78, b'0', 'Solo deportes', b'0', b'0', 'juliob', 13),
(79, NULL, 'Escuchar mas tarde', b'1', b'0', 'cristy257', 13),
(80, NULL, 'Deporte total', b'1', b'0', 'cristy257', 13),
(81, NULL, 'Novedades generales', b'1', b'0', 'cristy257', 13);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `lista_video`
--

CREATE TABLE `lista_video` (
  `Lista_id` int(11) NOT NULL,
  `videos_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `lista_video`
--

INSERT INTO `lista_video` (`Lista_id`, `videos_id`) VALUES
(73, 54),
(73, 53),
(73, 58),
(74, 57),
(74, 56),
(74, 55),
(75, 68),
(75, 69),
(75, 70),
(76, 54),
(76, 53),
(76, 62),
(76, 72),
(77, 66),
(77, 68),
(78, 58),
(78, 72);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario`
--

CREATE TABLE `usuario` (
  `nickname` varchar(255) NOT NULL,
  `apellido` varchar(255) DEFAULT NULL,
  `correo` varchar(255) DEFAULT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `fnacimiento` datetime DEFAULT NULL,
  `img` varchar(255) DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `usuario`
--

INSERT INTO `usuario` (`nickname`, `apellido`, `correo`, `deleted`, `fnacimiento`, `img`, `nombre`, `password`) VALUES
('cachilas', 'Cachila', 'Cachila.sil@c1080.org.uy', b'0', '1947-01-01 00:00:00', 'C:\\Users\\emiib\\Documents\\servidorfinal\\resources\\cachilas\\', 'Waldemar ', 'Sancho456'),
('chino', 'Recoba', 'chino@trico.com.uy', b'0', '1976-03-17 00:00:00', 'C:\\Users\\emiib\\Documents\\servidorfinal\\resources\\chino\\', 'Alvaro', 'Laika765'),
('crisy257', 'Crócamo', 'cris@cris.com', b'0', '1972-06-27 00:00:00', '22_07_19_cristina.jpg_1917179645.jpg', 'Cristina', '123123'),
('diegop', 'Parodi', 'diego@efectocine.com', b'0', '1975-01-01 00:00:00', 'C:\\Users\\emiib\\Documents\\servidorfinal\\resources\\diegop\\', 'Diego', 'Ruffo678'),
('emiib', 'Bayaona', 'emi@', b'0', NULL, 'C:\\Users\\emiib\\Documents\\servidorfinal\\resources\\emiib\\', 'Emiliano', '123123'),
('emito', 'Bay', 'emii22@asd', b'0', '1994-05-16 00:00:00', '', 'Emi', '123123'),
('hectorg', 'Guido', 'hector.gui@elgalpon.org.uy', b'0', '1954-01-07 00:00:00', 'C:\\Users\\emiib\\Documents\\servidorfinal\\resources\\hectorg\\', 'Héctor', 'Poncho345'),
('hrubino', 'Rubino', 'horacio.rubino@guambia.com.uy', b'0', '1962-02-25 00:00:00', 'http://bit.ly/2idUfBi', 'Horacio', 'Rufus123'),
('juliob', 'Bocca', 'juliobocca@sodre.com.uy', b'0', '1967-03-16 00:00:00', 'C:\\Users\\emiib\\Documents\\servidorfinal\\resources\\juliob\\', 'Julio', 'Salome56'),
('kairoh', 'Herrera', 'kairoher@pilserock.com.uy', b'0', '1840-04-25 00:00:00', 'C:\\Users\\emiib\\Documents\\servidorfinal\\resources\\kairoh\\', 'Kairo', 'Corbata15'),
('marcelot', 'Tinelli', 'marcelot@ideasdelsur.com.ar', b'0', '1960-04-01 00:00:00', 'C:\\Users\\emiib\\Documents\\servidorfinal\\resources\\marcelot\\', 'Marcelo', 'Mancha890'),
('mbusca', 'Buscaglia', 'Martin.bus@agadu.org.uy', b'0', '1972-06-14 00:00:00', 'C:\\Users\\emiib\\Documents\\servidorfinal\\resources\\mbusca\\', 'Martín', 'Cookie234'),
('nicoJ', 'Jodal', 'jodal@artech.com.uy', b'0', '1950-08-09 00:00:00', 'C:\\Users\\emiib\\Documents\\servidorfinal\\resources\\nicoJ\\', 'Nicolás', 'Albino80'),
('novick', 'Novick', 'edgardo@novick.com.uy', b'0', '1952-07-17 00:00:00', 'C:\\Users\\emiib\\Documents\\servidorfinal\\resources\\novick\\', 'Edgardo', 'Xenon987'),
('robinh', 'Henderson', 'Robin.h@tinglesa.com.uy', b'0', '1940-08-03 00:00:00', 'C:\\Users\\emiib\\Documents\\servidorfinal\\resources\\robinh\\', 'Robin', 'Aquiles67'),
('sergiop', 'Puglia', 'puglia@alpanpan.com.uy', b'0', '1950-01-28 00:00:00', 'C:\\Users\\emiib\\Documents\\servidorfinal\\resources\\sergiop\\', 'Sergio', 'Sultan876'),
('tabarec', 'Cardozo', ' tabare.car@agadu.org.uy', b'0', '1971-07-24 00:00:00', 'C:\\Users\\emiib\\Documents\\servidorfinal\\resources\\tabarec\\', 'Tabaré', 'Ketchup1'),
('tonyp', 'Pacheco', 'eltony@manya.com.uy', b'0', '1955-02-14 00:00:00', 'C:\\Users\\emiib\\Documents\\servidorfinal\\resources\\tonyp\\', 'Antonio', 'Kitty543');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuario_canal`
--

CREATE TABLE `usuario_canal` (
  `Usuario_nickname` varchar(255) NOT NULL,
  `canalesSeguidos_nombre` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `valoracionvideo`
--

CREATE TABLE `valoracionvideo` (
  `id` int(11) NOT NULL,
  `valoracion` int(11) DEFAULT NULL,
  `usuario_nickname` varchar(255) DEFAULT NULL,
  `video_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `video`
--

CREATE TABLE `video` (
  `id` int(11) NOT NULL,
  `deleted` bit(1) DEFAULT NULL,
  `descripcion` varchar(255) DEFAULT NULL,
  `duracion` varchar(255) DEFAULT NULL,
  `es_publico` bit(1) DEFAULT NULL,
  `fecha` datetime DEFAULT NULL,
  `nombre` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `visitas` varchar(255) DEFAULT NULL,
  `canal_nombre` varchar(255) DEFAULT NULL,
  `categoria_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Volcado de datos para la tabla `video`
--

INSERT INTO `video` (`id`, `deleted`, `descripcion`, `duracion`, `es_publico`, `fecha`, `nombre`, `url`, `visitas`, `canal_nombre`, `categoria_id`) VALUES
(5, b'0', 'ASD', '4:13', b'0', '2019-11-23 18:44:34', 'Regard - Ride It', 'https://youtu.be/-BwJ7JsxeIs', '949087', 'emiib', 13),
(7, b'0', '123', '4:13', b'0', '2019-11-23 18:45:10', 'PSY - GANGNAM STYLE(?????) M/V', 'https://youtu.be/9bZkp7q19f0', '3462421093', 'emiib', 1),
(53, b'0', 'Lindo video', '4:19', b'0', '2019-11-23 19:52:59', 'Niño Payaso Tabaré Cardozo.', 'https://youtu.be/K-uEIUnyZPg', '15211', 'tabarec', 1),
(54, b'0', '', '3:05', b'0', '2019-11-23 19:59:13', 'Tabaré Cardozo y Yamandú Cardozo - Locura Celeste (Tema oficial de Canal 4)', 'https://youtu.be/PAfbzKcePx0', '18367', 'tabarec', 1),
(55, b'0', '   ', '13:42', b'1', '2019-11-23 20:01:58', 'Michael Jackson - Thriller (Official Video)', 'https://youtu.be/sOnqjkJTMaA', '632690545', 'kairoh', 1),
(56, b'0', '', '3:59', b'0', '2019-11-23 20:04:08', 'Bruce Springsteen - Dancing In the Dark', 'https://youtu.be/129kuDCQtHs', '155803289', 'kairoh', 1),
(57, b'0', '', '5:03', b'0', '2019-11-23 20:04:31', 'Guns N\' Roses - Sweet Child O\' Mine (Official Music Video)', 'https://youtu.be/1w7OgIMMRc4', '1019482114', 'kairoh', 1),
(58, b'0', '', '4:24', b'0', '2019-11-23 20:05:17', '#Clausura2018 | Show de goles de la fecha 1 de Primera División', 'https://youtu.be/g46w4_kD_lA', '1268', 'juliob', 2),
(59, b'0', '', '5:49', b'0', '2019-11-23 20:07:19', 'Los goles más recordados de Antonio Pacheco', 'https://youtu.be/wlEd6-HsIxI', '61255', 'tonyp', 2),
(61, b'0', '', '51:39', b'0', '2019-11-23 20:23:57', '2A ETAPA DON TIMOTEO LIGUILLA', 'https://youtu.be/I_spHBU9ZsI', '24665', 'cachilas', 3),
(62, b'0', '', '57:16', b'0', '2019-11-23 20:24:18', '4A ETAPA A CONTRAMANO LIGUILLA', 'https://youtu.be/Es6GRMHXeCQ', '7558', 'cachilas', 3),
(65, b'0', '', '5:40', b'0', '2019-11-23 20:54:17', 'Entrevista realizada al director de CUTI', 'https://youtu.be/Eq5uBEzI6qs', '73', 'nicoJ', 8),
(66, b'0', '', '19:22', b'0', '2019-11-23 20:57:45', 'Ventana al Futuro Uruguay y el déficit de ingenieros', 'https://youtu.be/zBR2pnASlQE', '2814', 'nicoJ', 8),
(67, b'0', '', '13:37', b'0', '2019-11-23 21:01:00', 'Recoba - Top 20 Goals', 'https://youtu.be/Gy3fZhWdLEQ', '2376152', 'chino', 2),
(68, b'0', '', '1:01', b'0', '2019-11-23 21:03:01', 'Videoclip Ingeniería deMuestra 2017', 'https://youtu.be/RnaYRA1k5j4', '666', 'hectorg', 4),
(69, b'0', '', '27:23', b'0', '2019-11-23 21:03:12', 'Facultad de Ingeniería | Festejos por los 50 a?os del Instituto de Computación', 'https://youtu.be/GzOJSk4urlM', '304', 'hectorg', 4),
(70, b'0', '', '6:27', b'0', '2019-11-23 21:03:23', 'Facultad de Ingeniería - 100 a?os de su denominación', 'https://youtu.be/peGS4TBxSaI', '282', 'hectorg', 4),
(72, b'0', '', '58:43', b'0', '2019-11-23 21:06:53', 'FIESTA DEL SIGLO || Inauguración Estadio Pe?arol', 'https://youtu.be/SeVXVaF_ueA', '4028', 'tonyp', 2),
(82, b'0', '', '59:54', b'0', '2019-11-23 21:50:40', 'No Te Va Gustar - \"Otras Canciones 2019\" (Show Acústico Completo)', 'https://youtu.be/Yq-Kfc81h5s', '2416429', 'cristy257', 1),
(83, b'0', 'croquetas', '2:22', b'0', '2019-11-23 21:56:43', 'Croquetas de papa por Hugo Soca', 'https://youtu.be/z4PdOeN4x9E', '3114', 'cristy257', 6),
(84, b'0', '', '9:19', b'0', '2019-11-23 21:59:20', 'Jijiji + Mi perro dinamita (Indio Solari en Olavarría, 11-03-2017) versión DVD - HD / CC', 'https://youtu.be/kOhgFyy0598', '7271828', 'cristy257', 1),
(85, b'0', '', '4:25', b'0', '2019-11-23 22:06:02', 'AC/DC - You Shook Me All Night Long (from Live at River Plate)', 'https://youtu.be/zakKvbIQ28o', '95560596', 'cristy257', 13),
(86, b'0', '', '16:26', b'0', '2019-11-23 22:17:32', 'Reggaeton Old School Mix Bolichero (En Vivo)   Nico Vallorani DJ', 'https://youtu.be/A8ZxNcb_Vzk', '451990', 'emiib', 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `canal`
--
ALTER TABLE `canal`
  ADD PRIMARY KEY (`nombre`),
  ADD KEY `FKr68vuatjahcrq0hjqp2ie9u33` (`usuario_nickname`);

--
-- Indices de la tabla `categoria`
--
ALTER TABLE `categoria`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `comentario`
--
ALTER TABLE `comentario`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK8ml0o19yklqbplnjt0wqac1vf` (`usuario_nickname`),
  ADD KEY `FK4cw21rqxcjbhuh4dxiiee0u1k` (`vid_id`);

--
-- Indices de la tabla `comentario_comentario`
--
ALTER TABLE `comentario_comentario`
  ADD KEY `FKqack4f55a31h9nkw7yavih10k` (`respuestas_id`),
  ADD KEY `FK97aahv147tveifpc2e5tdta9` (`Comentario_id`);

--
-- Indices de la tabla `lista`
--
ALTER TABLE `lista`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FK1vkjxkbwg0ikfaymekf6t2w1i` (`canal_nombre`),
  ADD KEY `FK7p34u32gug0h5ng6o0hhxj3y9` (`categoria_id`);

--
-- Indices de la tabla `lista_video`
--
ALTER TABLE `lista_video`
  ADD KEY `FKp9kb937apcemdo9lhu3yinuer` (`videos_id`),
  ADD KEY `FKfe7l5t5grpslv3yumk6qbafk` (`Lista_id`);

--
-- Indices de la tabla `usuario`
--
ALTER TABLE `usuario`
  ADD PRIMARY KEY (`nickname`);

--
-- Indices de la tabla `usuario_canal`
--
ALTER TABLE `usuario_canal`
  ADD KEY `FK3cs35ibflevggvwyfrmhqq0dg` (`canalesSeguidos_nombre`),
  ADD KEY `FKlewtfkx26qsk69p7ab0iys2cy` (`Usuario_nickname`);

--
-- Indices de la tabla `valoracionvideo`
--
ALTER TABLE `valoracionvideo`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKe8ytcbq5wpa2o26ew2dro5j8s` (`usuario_nickname`),
  ADD KEY `FKsjqovecqbh6biueh04wwof0rh` (`video_id`);

--
-- Indices de la tabla `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`),
  ADD KEY `FKnt538fwr3rvtxfdxmt927dlom` (`canal_nombre`),
  ADD KEY `FK7549ix767xjq3y58d12i3p2e7` (`categoria_id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `categoria`
--
ALTER TABLE `categoria`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `canal`
--
ALTER TABLE `canal`
  ADD CONSTRAINT `FKr68vuatjahcrq0hjqp2ie9u33` FOREIGN KEY (`usuario_nickname`) REFERENCES `usuario` (`nickname`);

--
-- Filtros para la tabla `comentario`
--
ALTER TABLE `comentario`
  ADD CONSTRAINT `FK4cw21rqxcjbhuh4dxiiee0u1k` FOREIGN KEY (`vid_id`) REFERENCES `video` (`id`),
  ADD CONSTRAINT `FK8ml0o19yklqbplnjt0wqac1vf` FOREIGN KEY (`usuario_nickname`) REFERENCES `usuario` (`nickname`);

--
-- Filtros para la tabla `comentario_comentario`
--
ALTER TABLE `comentario_comentario`
  ADD CONSTRAINT `FK97aahv147tveifpc2e5tdta9` FOREIGN KEY (`Comentario_id`) REFERENCES `comentario` (`id`),
  ADD CONSTRAINT `FKqack4f55a31h9nkw7yavih10k` FOREIGN KEY (`respuestas_id`) REFERENCES `comentario` (`id`);

--
-- Filtros para la tabla `lista`
--
ALTER TABLE `lista`
  ADD CONSTRAINT `FK1vkjxkbwg0ikfaymekf6t2w1i` FOREIGN KEY (`canal_nombre`) REFERENCES `canal` (`nombre`),
  ADD CONSTRAINT `FK7p34u32gug0h5ng6o0hhxj3y9` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id`);

--
-- Filtros para la tabla `lista_video`
--
ALTER TABLE `lista_video`
  ADD CONSTRAINT `FKfe7l5t5grpslv3yumk6qbafk` FOREIGN KEY (`Lista_id`) REFERENCES `lista` (`id`),
  ADD CONSTRAINT `FKp9kb937apcemdo9lhu3yinuer` FOREIGN KEY (`videos_id`) REFERENCES `video` (`id`);

--
-- Filtros para la tabla `usuario_canal`
--
ALTER TABLE `usuario_canal`
  ADD CONSTRAINT `FK3cs35ibflevggvwyfrmhqq0dg` FOREIGN KEY (`canalesSeguidos_nombre`) REFERENCES `canal` (`nombre`),
  ADD CONSTRAINT `FKlewtfkx26qsk69p7ab0iys2cy` FOREIGN KEY (`Usuario_nickname`) REFERENCES `usuario` (`nickname`);

--
-- Filtros para la tabla `valoracionvideo`
--
ALTER TABLE `valoracionvideo`
  ADD CONSTRAINT `FKe8ytcbq5wpa2o26ew2dro5j8s` FOREIGN KEY (`usuario_nickname`) REFERENCES `usuario` (`nickname`),
  ADD CONSTRAINT `FKsjqovecqbh6biueh04wwof0rh` FOREIGN KEY (`video_id`) REFERENCES `video` (`id`);

--
-- Filtros para la tabla `video`
--
ALTER TABLE `video`
  ADD CONSTRAINT `FK7549ix767xjq3y58d12i3p2e7` FOREIGN KEY (`categoria_id`) REFERENCES `categoria` (`id`),
  ADD CONSTRAINT `FKnt538fwr3rvtxfdxmt927dlom` FOREIGN KEY (`canal_nombre`) REFERENCES `canal` (`nombre`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
